#pragma once

#include "nTypes.h"
#include "Scheduler.h"
#include "FitMath.h"
#include "ArrayGen.h"
#include "../oldfit/Predict.h"

template<class T>
void ntransportCovariance (
  const TrackMatrix& tm,
  const T& uc,
  nTrackSymMatrix& pc
) {
  bool isLine = tm(0,4)==0;
  if (!isLine) {
    math_similarity_5_5_nonvec(tm, uc, pc);
  } else {
    pc.copy(uc);

    if (tm(0,2) != 0 or tm(1,3) != 0) {
      pc(0,0) += 2 * uc(2,0) * tm(0,2) + uc(2,2) * tm(0,2) * tm(0,2);
      pc(2,0) += uc(2,2) * tm(0,2);
      pc(1,1) += 2 * uc(3,1) * tm(1,3) + uc(3,3) * tm(1,3) * tm(1,3);
      pc(3,1) += uc(3,3) * tm(1,3);
      pc(1,0) += uc(2,1) * tm(0,2) + uc(3,0) * tm(1,3) + uc(3,2) * tm(0,2) * tm(1,3);
      pc(2,1) += uc(3,2) * tm(1,3);
      pc(3,0) += uc(3,2) * tm(0,2);
    }
  }
}

template<class T>
void npredictInitialise (
  nFitNode& node,
  const TrackSymMatrix& covariance
) {
  node.getState<T, Fit::Predicted, Fit::StateVector>().copy(node.m_refVector.m_parameters);
  node.getState<T, Fit::Predicted, Fit::Covariance>().copy(covariance);
}

template<class T, bool U>
void npredict (
  nFitNode& node,
  const nFitNode& prevnode
);

template<class T>
struct npredict_vec {
  template<unsigned W>
  static void op (
    std::array<SchItem, W>& n,
    double_ptr_64_const us,
    double_ptr_64_const uc,
    double_ptr_64 ps,
    double_ptr_64 pc
  );
};

// Vectorised predicts that assume data is already prepared, aligned and so on

template<>
struct npredict_vec<Fit::Forward> {
  template<unsigned W>
  static void op (
    std::array<SchItem, W>& n,
    double_ptr_64_const us,
    double_ptr_64_const uc,
    double_ptr_64 ps,
    double_ptr_64 pc
  ) {
    const std::array<double, 25*W> _aligned tm = ArrayGen::getCurrentTransportMatrix(n);
    const std::array<double, 15*W> _aligned nm = ArrayGen::getCurrentNoiseMatrix(n);
    const std::array<double, 5*W> _aligned tv = ArrayGen::getCurrentTransportVector(n);

    FitMath<Fit::Forward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    FitMath<Fit::Forward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

template<>
struct npredict_vec<Fit::Backward> {
  template<unsigned W>
  static void op (
    std::array<SchItem, W>& n,
    double_ptr_64_const us,
    double_ptr_64_const uc,
    double_ptr_64 ps,
    double_ptr_64 pc
  ) {
    const std::array<double, 5*W> _aligned tv = ArrayGen::getPreviousTransportVector(n);
    const std::array<double, 25*W> _aligned tm = ArrayGen::getPreviousTransportMatrix(n);
    std::array<double, 15*W> _aligned nm = ArrayGen::getPreviousNoiseMatrix(n);

    FitMath<Fit::Backward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    FitMath<Fit::Backward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};
