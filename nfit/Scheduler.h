#pragma once

#include <numeric>
#include <tuple>
#include "nTypes.h"
#include "../utilities/Tools.h"

struct SchItem {
  std::vector<nFitNode>* track;
  std::vector<nFitNode>::iterator prevnode;
  std::vector<nFitNode>::iterator node;
  std::vector<nFitNode>::iterator firstnode;
  std::vector<nFitNode>::iterator lastnode;
  unsigned int trackIndex;

  SchItem () = default;
  SchItem (const SchItem& copy) = default;
  SchItem (
    std::vector<nFitNode>* track,
    const unsigned int& prevnodeIndex,
    const unsigned int& nodeIndex,
    const unsigned int& backwardUpstream,
    const unsigned int& trackIndex
  ) : track(track), trackIndex(trackIndex)
  {
    prevnode  = track->begin() + prevnodeIndex;
    node      = track->begin() + nodeIndex;
    lastnode = track->end() - backwardUpstream - 1;
  }
};

struct DumbStaticScheduler {
  static std::vector<std::tuple<uint8_t, uint8_t, uint8_t, std::array<SchItem, VECTOR_WIDTH>>>
  generate (
    std::vector<nTrack>& ntracks,
    const bool showSchedule
  );
};

struct SwapStore {
  nStates& store;
  nTrackVector state;
  nTrackSymMatrix covariance;

  SwapStore (nStates& store, const nTrackVector& state, const nTrackSymMatrix& covariance) :
    store(store), state(state), covariance(covariance) {}
};
