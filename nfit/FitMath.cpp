#include "FitMath.h"

void math_update_nonvec (
  nTrackVector& X,
  nTrackSymMatrix& C,
  double& chi2,
  const double* Xref,
  const double* H,
  double refResidual,
  double errorMeas2
) {
  // The ugly code below makes the filter step about 20% faster
  // than SMatrix would do it.
  auto  res = refResidual +  H[0] * (Xref[0] - X[0]) 
                          +  H[1] * (Xref[1] - X[1])
                          +  H[2] * (Xref[2] - X[2])
                          +  H[3] * (Xref[3] - X[3]) 
                          +  H[4] * (Xref[4] - X[4]);
  double CHT[5] = {
   C[ 0]*H[0] + C[ 1]*H[1] + C[ 3]*H[2] + C[ 6]*H[3] + C[10]*H[4] ,
   C[ 1]*H[0] + C[ 2]*H[1] + C[ 4]*H[2] + C[ 7]*H[3] + C[11]*H[4] ,
   C[ 3]*H[0] + C[ 4]*H[1] + C[ 5]*H[2] + C[ 8]*H[3] + C[12]*H[4] ,
   C[ 6]*H[0] + C[ 7]*H[1] + C[ 8]*H[2] + C[ 9]*H[3] + C[13]*H[4] ,
   C[10]*H[0] + C[11]*H[1] + C[12]*H[2] + C[13]*H[3] + C[14]*H[4] 
  };
  auto errorResInv2  = 1.0 /
    (errorMeas2
    + H[0]*CHT[0]
    + H[1]*CHT[1]
    + H[2]*CHT[2]
    + H[3]*CHT[3]
    + H[4]*CHT[4]);

  // update the state vector and cov matrix
  auto w = res * errorResInv2;
  X[0] += CHT[0] * w;
  X[1] += CHT[1] * w;
  X[2] += CHT[2] * w;
  X[3] += CHT[3] * w;
  X[4] += CHT[4] * w;

  C[ 0] -= errorResInv2 * CHT[0] * CHT[0];
  C[ 1] -= errorResInv2 * CHT[1] * CHT[0];
  C[ 3] -= errorResInv2 * CHT[2] * CHT[0];
  C[ 6] -= errorResInv2 * CHT[3] * CHT[0];
  C[10] -= errorResInv2 * CHT[4] * CHT[0];

  C[ 2] -= errorResInv2 * CHT[1] * CHT[1];
  C[ 4] -= errorResInv2 * CHT[2] * CHT[1];
  C[ 7] -= errorResInv2 * CHT[3] * CHT[1];
  C[11] -= errorResInv2 * CHT[4] * CHT[1];

  C[ 5] -= errorResInv2 * CHT[2] * CHT[2];
  C[ 8] -= errorResInv2 * CHT[3] * CHT[2];
  C[12] -= errorResInv2 * CHT[4] * CHT[2];

  C[ 9] -= errorResInv2 * CHT[3] * CHT[3];
  C[13] -= errorResInv2 * CHT[4] * CHT[3];

  C[14] -= errorResInv2 * CHT[4] * CHT[4];

  chi2 = res * res * errorResInv2;
}
