#include "Smoother.h"

void updateResiduals (
  nFitNode& node
) {
  double value {0.0}, error {0.0};
  if (node.m_measurement != 0) {
    const TrackProjectionMatrix& H  = node.m_projectionMatrix;
    double HCH;
    math_similarity_5_1_nonvec (
      node.getSmooth<Fit::Covariance>(),
      H.fArray,
      &HCH);
    
    const TrackVector& refX = node.m_refVector.m_parameters;
    const double V = node.m_errMeasure * node.m_errMeasure;
    const double sign = node.m_type == HitOnTrack ? -1 : 1;
    value = node.m_refResidual + (H * (refX - node.getSmooth<Fit::StateVector>()));
    error = V + sign * HCH;
  }
  *node.m_smoothState.m_residual = value;
  *node.m_smoothState.m_errResidual = error;
}
