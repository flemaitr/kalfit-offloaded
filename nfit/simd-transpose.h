#pragma once

#ifdef __SSE__
/**
 * Transposition in SSE of 4 float vectors
 */
//#define _MM_TRANSPOSE4_PS(r0, r1, r2, r3) ...

/**
 * Transposition in SSE of 2 double vectors
 */
#define ___MM_TRANSPOSE2_PD(in0, in1, out0, out1, __in0, __in1, __out0, __out1) \
  do { \
    __m128d __in0 = (in0), __in1 = (in1); \
    __m128d __out0, __out1; \
    __out0 = _mm_shuffle_pd(__in0, __in1, 0x0); \
    __out1 = _mm_shuffle_pd(__in0, __in1, 0x3); \
    (out0) = __out0, (out1) = __out1; \
  } while (0)

#define _MM_TRANSPOSE2_PD(in0, in1) \
      ___MM_TRANSPOSE2_PD(in0, in1, in0, in1, __in0##__LINE__, __in1##__LINE__, __out0##__LINE__, __out1##__LINE__)

#endif //__SSE__


#ifdef __AVX__
/**
 * Transposition in AVX of 8 float vectors
 */
#define ___MM256_TRANSPOSE8_PS(in0, in1, in2, in3, in4, in5, in6, in7, out0, out1, out2, out3, out4, out5, out6, out7, __in0, __in1, __in2, __in3, __in4, __in5, __in6, __in7, __out0, __out1, __out2, __out3, __out4, __out5, __out6, __out7, __tmp0, __tmp1, __tmp2, __tmp3, __tmp4, __tmp5, __tmp6, __tmp7, __tmpp0, __tmpp1, __tmpp2, __tmpp3, __tmpp4, __tmpp5, __tmpp6, __tmpp7) \
  do { \
    __m256 __in0 = (in0), __in1 = (in1), __in2 = (in2), __in3 = (in3), __in4 = (in4), __in5 = (in5), __in6 = (in6), __in7 = (in7); \
    __m256 __tmp0, __tmp1, __tmp2, __tmp3, __tmp4, __tmp5, __tmp6, __tmp7; \
    __m256 __tmpp0, __tmpp1, __tmpp2, __tmpp3, __tmpp4, __tmpp5, __tmpp6, __tmpp7; \
    __m256 __out0, __out1, __out2, __out3, __out4, __out5, __out6, __out7; \
    __tmp0  = _mm256_unpacklo_ps(__in0, __in1); \
    __tmp1  = _mm256_unpackhi_ps(__in0, __in1); \
    __tmp2  = _mm256_unpacklo_ps(__in2, __in3); \
    __tmp3  = _mm256_unpackhi_ps(__in2, __in3); \
    __tmp4  = _mm256_unpacklo_ps(__in4, __in5); \
    __tmp5  = _mm256_unpackhi_ps(__in4, __in5); \
    __tmp6  = _mm256_unpacklo_ps(__in6, __in7); \
    __tmp7  = _mm256_unpackhi_ps(__in6, __in7); \
    __tmpp0 = _mm256_shuffle_ps(__tmp0, __tmp2, 0x44); \
    __tmpp1 = _mm256_shuffle_ps(__tmp0, __tmp2, 0xEE); \
    __tmpp2 = _mm256_shuffle_ps(__tmp1, __tmp3, 0x44); \
    __tmpp3 = _mm256_shuffle_ps(__tmp1, __tmp3, 0xEE); \
    __tmpp4 = _mm256_shuffle_ps(__tmp4, __tmp6, 0x44); \
    __tmpp5 = _mm256_shuffle_ps(__tmp4, __tmp6, 0xEE); \
    __tmpp6 = _mm256_shuffle_ps(__tmp5, __tmp7, 0x44); \
    __tmpp7 = _mm256_shuffle_ps(__tmp5, __tmp7, 0xEE); \
    __out0  = _mm256_permute2f128_ps(__tmpp0, __tmpp4, 0x20); \
    __out1  = _mm256_permute2f128_ps(__tmpp1, __tmpp5, 0x20); \
    __out2  = _mm256_permute2f128_ps(__tmpp2, __tmpp6, 0x20); \
    __out3  = _mm256_permute2f128_ps(__tmpp3, __tmpp7, 0x20); \
    __out4  = _mm256_permute2f128_ps(__tmpp0, __tmpp4, 0x31); \
    __out5  = _mm256_permute2f128_ps(__tmpp1, __tmpp5, 0x31); \
    __out6  = _mm256_permute2f128_ps(__tmpp2, __tmpp6, 0x31); \
    __out7  = _mm256_permute2f128_ps(__tmpp3, __tmpp7, 0x31); \
    (out0)  = __out0, (out1) = __out1, (out2) = __out2, (out3) = __out3, (out4) = __out4, (out5) = __out5, (out6) = __out6, (out7) = __out7; \
  } while (0)

#define _MM256_TRANSPOSE8_PS(in0, in1, in2, in3, in4, in5, in6, in7) \
      ___MM256_TRANSPOSE8_PS(in0, in1, in2, in3, in4, in5, in6, in7, in0, in1, in2, in3, in4, in5, in6, in7, \
          __in0##__LINE__, __in1##__LINE__, __in2##__LINE__, __in3##__LINE__, __in4##__LINE__, __in5##__LINE__, __in6##__LINE__, __in7##__LINE__, \
          __out0##__LINE__, __out1##__LINE__, __out2##__LINE__, __out3##__LINE__, __out4##__LINE__, __out5##__LINE__, __out6##__LINE__, __out7##__LINE__, \
          __tmp0##__LINE__, __tmp1##__LINE__, __tmp2##__LINE__, __tmp3##__LINE__, __tmp4##__LINE__, __tmp5##__LINE__, __tmp6##__LINE__, __tmp7##__LINE__, \
          __tmpp0##__LINE__, __tmpp1##__LINE__, __tmpp2##__LINE__, __tmpp3##__LINE__, __tmpp4##__LINE__, __tmpp5##__LINE__, __tmpp6##__LINE__, __tmpp7##__LINE__)


/**
 * Transposition in AVX of 4 double vectors
 */
#define ___MM256_TRANSPOSE4_PD(in0, in1, in2, in3, out0, out1, out2, out3, __in0, __in1, __in2, __in3, __out0, __out1, __out2, __out3, __tmp0, __tmp1, __tmp2, __tmp3) \
  do { \
    __m256d __in0 = (in0), __in1 = (in1), __in2 = (in2), __in3 = (in3); \
    __m256d __tmp0, __tmp1, __tmp2, __tmp3; \
    __m256d __out0, __out1, __out2, __out3; \
    __tmp0 = _mm256_shuffle_pd(__in0, __in1, 0x0); \
    __tmp1 = _mm256_shuffle_pd(__in0, __in1, 0xf); \
    __tmp2 = _mm256_shuffle_pd(__in2, __in3, 0x0); \
    __tmp3 = _mm256_shuffle_pd(__in2, __in3, 0xf); \
    __out0 = _mm256_permute2f128_pd(__tmp0, __tmp2, 0x20); \
    __out1 = _mm256_permute2f128_pd(__tmp1, __tmp3, 0x20); \
    __out2 = _mm256_permute2f128_pd(__tmp0, __tmp2, 0x31); \
    __out3 = _mm256_permute2f128_pd(__tmp1, __tmp3, 0x31); \
    (out0) = __out0, (out1) = __out1, (out2) = __out2, (out3) = __out3; \
  } while (0)

#define _MM256_TRANSPOSE4_PD(in0, in1, in2, in3) \
      ___MM256_TRANSPOSE4_PD(in0, in1, in2, in3, in0, in1, in2, in3, \
          __in0##__LINE__, __in1##__LINE__, __in2##__LINE__, __in3##__LINE__, \
          __out0##__LINE__, __out1##__LINE__, __out2##__LINE__, __out3##__LINE__, \
          __tmp0##__LINE__, __tmp1##__LINE__, __tmp2##__LINE__, __tmp3##__LINE__)

#endif //__AVX__

#ifdef __AVX512F__
/**
 * Transposition in AVX512 of 16 float vectors
 */
#define ___MM512_TRANSPOSE16_PS(in0, in1, in2, in3, in4, in5, in6, in7, in8, in9, inA, inB, inC, inD, inE, inF, out0, out1, out2, out3, out4, out5, out6, out7, out8, out9, outA, outB, outC, outD, outE, outF, __in0, __in1, __in2, __in3, __in4, __in5, __in6, __in7, __in8, __in9, __inA, __inB, __inC, __inD, __inE, __inF, __out0, __out1, __out2, __out3, __out4, __out5, __out6, __out7, __out8, __out9, __outA, __outB, __outC, __outD, __outE, __outF, __tmp0, __tmp1, __tmp2, __tmp3, __tmp4, __tmp5, __tmp6, __tmp7, __tmp8, __tmp9, __tmpA, __tmpB, __tmpC, __tmpD, __tmpE, __tmpF, __tmpp0, __tmpp1, __tmpp2, __tmpp3, __tmpp4, __tmpp5, __tmpp6, __tmpp7, __tmpp8, __tmpp9, __tmppA, __tmppB, __tmppC, __tmppD, __tmpppE, __tmppF) \
  do { \
    __m512i __in0 = _mm512_castps_si512(in0), __in1 = _mm512_castps_si512(in1), __in2 = _mm512_castps_si512(in2), __in3 = _mm512_castps_si512(in3), __in4 = _mm512_castps_si512(in4), __in5 = _mm512_castps_si512(in5), __in6 = _mm512_castps_si512(in6), __in7 = _mm512_castps_si512(in7), __in8 = _mm512_castps_si512(in8), __in9 = _mm512_castps_si512(in9), __inA = _mm512_castps_si512(inA), __inB = _mm512_castps_si512(inB), __inC = _mm512_castps_si512(inC), __inD = _mm512_castps_si512(inD), __inE = _mm512_castps_si512(inE), __inF = _mm512_castps_si512(inF); \
    __m512i __tmp0, __tmp1, __tmp2, __tmp3, __tmp4, __tmp5, __tmp6, __tmp7, __tmp8, __tmp9, __tmpA, __tmpB, __tmpC, __tmpD, __tmpE, __tmpF; \
    __m512i __tmpp0, __tmpp1, __tmpp2, __tmpp3, __tmpp4, __tmpp5, __tmpp6, __tmpp7, __tmpp8, __tmpp9, __tmppA, __tmppB, __tmppC, __tmppD, __tmppE, __tmppF; \
    __m512i __out0, __out1, __out2, __out3, __out4, __out5, __out6, __out7, __out8, __out9, __outA, __outB, __outC, __outD, __outE, __outF; \
     \
    __tmp0 = _mm512_unpacklo_epi32(__in0,__in1); \
    __tmp1 = _mm512_unpackhi_epi32(__in0,__in1); \
    __tmp2 = _mm512_unpacklo_epi32(__in2,__in3); \
    __tmp3 = _mm512_unpackhi_epi32(__in2,__in3); \
    __tmp4 = _mm512_unpacklo_epi32(__in4,__in5); \
    __tmp5 = _mm512_unpackhi_epi32(__in4,__in5); \
    __tmp6 = _mm512_unpacklo_epi32(__in6,__in7); \
    __tmp7 = _mm512_unpackhi_epi32(__in6,__in7); \
    __tmp8 = _mm512_unpacklo_epi32(__in8,__in9); \
    __tmp9 = _mm512_unpackhi_epi32(__in8,__in9); \
    __tmpA = _mm512_unpacklo_epi32(__inA,__inB); \
    __tmpB = _mm512_unpackhi_epi32(__inA,__inB); \
    __tmpC = _mm512_unpacklo_epi32(__inC,__inD); \
    __tmpD = _mm512_unpackhi_epi32(__inC,__inD); \
    __tmpE = _mm512_unpacklo_epi32(__inE,__inF); \
    __tmpF = _mm512_unpackhi_epi32(__inE,__inF); \
     \
    __tmpp0 = _mm512_unpacklo_epi64(__tmp0,__tmp2); \
    __tmpp1 = _mm512_unpackhi_epi64(__tmp0,__tmp2); \
    __tmpp2 = _mm512_unpacklo_epi64(__tmp1,__tmp3); \
    __tmpp3 = _mm512_unpackhi_epi64(__tmp1,__tmp3); \
    __tmpp4 = _mm512_unpacklo_epi64(__tmp4,__tmp6); \
    __tmpp5 = _mm512_unpackhi_epi64(__tmp4,__tmp6); \
    __tmpp6 = _mm512_unpacklo_epi64(__tmp5,__tmp7); \
    __tmpp7 = _mm512_unpackhi_epi64(__tmp5,__tmp7); \
    __tmpp8 = _mm512_unpacklo_epi64(__tmp8,__tmpA); \
    __tmpp9 = _mm512_unpackhi_epi64(__tmp8,__tmpA); \
    __tmppA = _mm512_unpacklo_epi64(__tmp9,__tmpB); \
    __tmppB = _mm512_unpackhi_epi64(__tmp9,__tmpB); \
    __tmppC = _mm512_unpacklo_epi64(__tmpC,__tmpE); \
    __tmppD = _mm512_unpackhi_epi64(__tmpC,__tmpE); \
    __tmppE = _mm512_unpacklo_epi64(__tmpD,__tmpF); \
    __tmppF = _mm512_unpackhi_epi64(__tmpD,__tmpF); \
     \
    __tmp0 = _mm512_shuffle_i32x4(__tmpp0, __tmpp4, 0x88); \
    __tmp1 = _mm512_shuffle_i32x4(__tmpp1, __tmpp5, 0x88); \
    __tmp2 = _mm512_shuffle_i32x4(__tmpp2, __tmpp6, 0x88); \
    __tmp3 = _mm512_shuffle_i32x4(__tmpp3, __tmpp7, 0x88); \
    __tmp4 = _mm512_shuffle_i32x4(__tmpp0, __tmpp4, 0xdd); \
    __tmp5 = _mm512_shuffle_i32x4(__tmpp1, __tmpp5, 0xdd); \
    __tmp6 = _mm512_shuffle_i32x4(__tmpp2, __tmpp6, 0xdd); \
    __tmp7 = _mm512_shuffle_i32x4(__tmpp3, __tmpp7, 0xdd); \
    __tmp8 = _mm512_shuffle_i32x4(__tmpp8, __tmppC, 0x88); \
    __tmp9 = _mm512_shuffle_i32x4(__tmpp9, __tmppD, 0x88); \
    __tmpA = _mm512_shuffle_i32x4(__tmppA, __tmppE, 0x88); \
    __tmpB = _mm512_shuffle_i32x4(__tmppB, __tmppF, 0x88); \
    __tmpC = _mm512_shuffle_i32x4(__tmpp8, __tmppC, 0xdd); \
    __tmpD = _mm512_shuffle_i32x4(__tmpp9, __tmppD, 0xdd); \
    __tmpE = _mm512_shuffle_i32x4(__tmppA, __tmppE, 0xdd); \
    __tmpF = _mm512_shuffle_i32x4(__tmppB, __tmppF, 0xdd); \
     \
    __out0 = _mm512_shuffle_i32x4(__tmp0, __tmp8, 0x88); \
    __out1 = _mm512_shuffle_i32x4(__tmp1, __tmp9, 0x88); \
    __out2 = _mm512_shuffle_i32x4(__tmp2, __tmpA, 0x88); \
    __out3 = _mm512_shuffle_i32x4(__tmp3, __tmpB, 0x88); \
    __out4 = _mm512_shuffle_i32x4(__tmp4, __tmpC, 0x88); \
    __out5 = _mm512_shuffle_i32x4(__tmp5, __tmpD, 0x88); \
    __out6 = _mm512_shuffle_i32x4(__tmp6, __tmpE, 0x88); \
    __out7 = _mm512_shuffle_i32x4(__tmp7, __tmpF, 0x88); \
    __out8 = _mm512_shuffle_i32x4(__tmp0, __tmp8, 0xdd); \
    __out9 = _mm512_shuffle_i32x4(__tmp1, __tmp9, 0xdd); \
    __outA = _mm512_shuffle_i32x4(__tmp2, __tmpA, 0xdd); \
    __outB = _mm512_shuffle_i32x4(__tmp3, __tmpB, 0xdd); \
    __outC = _mm512_shuffle_i32x4(__tmp4, __tmpC, 0xdd); \
    __outD = _mm512_shuffle_i32x4(__tmp5, __tmpD, 0xdd); \
    __outE = _mm512_shuffle_i32x4(__tmp6, __tmpE, 0xdd); \
    __outF = _mm512_shuffle_i32x4(__tmp7, __tmpF, 0xdd); \
     \
    (out0)  = _mm512_castsi512_ps(__out0), (out1) = _mm512_castsi512_ps(__out1), (out2) = _mm512_castsi512_ps(__out2), (out3) = _mm512_castsi512_ps(__out3), (out4) = _mm512_castsi512_ps(__out4), (out5) = _mm512_castsi512_ps(__out5), (out6) = _mm512_castsi512_ps(__out6), (out7) = _mm512_castsi512_ps(__out7), (out8) = _mm512_castsi512_ps(__out8), (out9) = _mm512_castsi512_ps(__out9), (outA) = _mm512_castsi512_ps(__outA), (outB) = _mm512_castsi512_ps(__outB), (outC) = _mm512_castsi512_ps(__outC), (outD) = _mm512_castsi512_ps(__outD), (outE) = _mm512_castsi512_ps(__outE), (outF) = _mm512_castsi512_ps(__outF); \
  } while (0)

#define _MM512_TRANSPOSE16_PS(in0, in1, in2, in3, in4, in5, in6, in7, in8, in9, inA, inB, inC, inD, inE, inF) \
      ___MM512_TRANSPOSE16_PS(in0, in1, in2, in3, in4, in5, in6, in7, in8, in9, inA, inB, inC, inD, inE, inF, in0, in1, in2, in3, in4, in5, in6, in7, in8, in9, inA, inB, inC, inD, inE, inF, \
          __in0##__LINE__, __in1##__LINE__, __in2##__LINE__, __in3##__LINE__, __in4##__LINE__, __in5##__LINE__, __in6##__LINE__, __in7##__LINE__, __in8##__LINE__, __in9##__LINE__, __inA##__LINE__, __inB##__LINE__, __inC##__LINE__, __inD##__LINE__, __inE##__LINE__, __inF##__LINE__, \
          __out0##__LINE__, __out1##__LINE__, __out2##__LINE__, __out3##__LINE__, __out4##__LINE__, __out5##__LINE__, __out6##__LINE__, __out7##__LINE__, __out8##__LINE__, __out9##__LINE__, __outA##__LINE__, __outB##__LINE__, __outC##__LINE__, __outD##__LINE__, __outE##__LINE__, __outF##__LINE__, \
          __tmp0##__LINE__, __tmp1##__LINE__, __tmp2##__LINE__, __tmp3##__LINE__, __tmp4##__LINE__, __tmp5##__LINE__, __tmp6##__LINE__, __tmp7##__LINE__, __tmp8##__LINE__, __tmp9##__LINE__, __tmpA##__LINE__, __tmpB##__LINE__, __tmpC##__LINE__, __tmpD##__LINE__, __tmpE##__LINE__, __tmpF##__LINE__, \
          __tmpp0##__LINE__, __tmpp1##__LINE__, __tmpp2##__LINE__, __tmpp3##__LINE__, __tmpp4##__LINE__, __tmpp5##__LINE__, __tmpp6##__LINE__, __tmpp7##__LINE__, __tmpp8##__LINE__, __tmpp9##__LINE__, __tmppA##__LINE__, __tmppB##__LINE__, __tmppC##__LINE__, __tmppD##__LINE__, __tmppE##__LINE__, __tmppF##__LINE__)



/**
 * Transposition in AVX512 of 8 double vectors
 */
#define ___MM512_TRANSPOSE8_PD(in0, in1, in2, in3, in4, in5, in6, in7, out0, out1, out2, out3, out4, out5, out6, out7, __in0, __in1, __in2, __in3, __in4, __in5, __in6, __in7, __out0, __out1, __out2, __out3, __out4, __out5, __out6, __out7, __tmp0, __tmp1, __tmp2, __tmp3, __tmp4, __tmp5, __tmp6, __tmp7, __tmpp0, __tmpp1, __tmpp2, __tmpp3, __tmpp4, __tmpp5, __tmpp6, __tmpp7) \
  do { \
    __m512i __in0 = _mm512_castpd_si512(in0), __in1 = _mm512_castpd_si512(in1), __in2 = _mm512_castpd_si512(in2), __in3 = _mm512_castpd_si512(in3), __in4 = _mm512_castpd_si512(in4), __in5 = _mm512_castpd_si512(in5), __in6 = _mm512_castpd_si512(in6), __in7 = _mm512_castpd_si512(in7); \
    __m512i __tmp0, __tmp1, __tmp2, __tmp3, __tmp4, __tmp5, __tmp6, __tmp7; \
    __m512i __tmpp0, __tmpp1, __tmpp2, __tmpp3, __tmpp4, __tmpp5, __tmpp6, __tmpp7; \
    __m512i __out0, __out1, __out2, __out3, __out4, __out5, __out6, __out7; \
     \
    __tmp0  = _mm512_unpacklo_epi64(__in0,__in1); \
    __tmp1  = _mm512_unpackhi_epi64(__in0,__in1); \
    __tmp2  = _mm512_unpacklo_epi64(__in2,__in3); \
    __tmp3  = _mm512_unpackhi_epi64(__in2,__in3); \
    __tmp4  = _mm512_unpacklo_epi64(__in4,__in5); \
    __tmp5  = _mm512_unpackhi_epi64(__in4,__in5); \
    __tmp6  = _mm512_unpacklo_epi64(__in6,__in7); \
    __tmp7  = _mm512_unpackhi_epi64(__in6,__in7); \
     \
    __tmpp0 = _mm512_shuffle_i64x2(__tmp0, __tmp2, 0x88); \
    __tmpp1 = _mm512_shuffle_i64x2(__tmp1, __tmp3, 0x88); \
    __tmpp2 = _mm512_shuffle_i64x2(__tmp0, __tmp2, 0xdd); \
    __tmpp3 = _mm512_shuffle_i64x2(__tmp1, __tmp3, 0xdd); \
    __tmpp4 = _mm512_shuffle_i64x2(__tmp4, __tmp6, 0x88); \
    __tmpp5 = _mm512_shuffle_i64x2(__tmp5, __tmp7, 0x88); \
    __tmpp6 = _mm512_shuffle_i64x2(__tmp4, __tmp6, 0xdd); \
    __tmpp7 = _mm512_shuffle_i64x2(__tmp5, __tmp7, 0xdd); \
     \
    __out0  = _mm512_shuffle_i64x2(__tmpp0, __tmpp4, 0x88); \
    __out1  = _mm512_shuffle_i64x2(__tmpp1, __tmpp5, 0x88); \
    __out2  = _mm512_shuffle_i64x2(__tmpp2, __tmpp6, 0x88); \
    __out3  = _mm512_shuffle_i64x2(__tmpp3, __tmpp7, 0x88); \
    __out4  = _mm512_shuffle_i64x2(__tmpp0, __tmpp4, 0xdd); \
    __out5  = _mm512_shuffle_i64x2(__tmpp1, __tmpp5, 0xdd); \
    __out6  = _mm512_shuffle_i64x2(__tmpp2, __tmpp6, 0xdd); \
    __out7  = _mm512_shuffle_i64x2(__tmpp3, __tmpp7, 0xdd); \
     \
    (out0)  = _mm512_castsi512_pd(__out0), (out1) = _mm512_castsi512_pd(__out1), (out2) = _mm512_castsi512_pd(__out2), (out3) = _mm512_castsi512_pd(__out3), (out4) = _mm512_castsi512_pd(__out4), (out5) = _mm512_castsi512_pd(__out5), (out6) = _mm512_castsi512_pd(__out6), (out7) = _mm512_castsi512_pd(__out7); \
  } while (0)

#define _MM512_TRANSPOSE8_PD(in0, in1, in2, in3, in4, in5, in6, in7) \
      ___MM512_TRANSPOSE8_PD(in0, in1, in2, in3, in4, in5, in6, in7, in0, in1, in2, in3, in4, in5, in6, in7, \
          __in0##__LINE__, __in1##__LINE__, __in2##__LINE__, __in3##__LINE__, __in4##__LINE__, __in5##__LINE__, __in6##__LINE__, __in7##__LINE__, \
          __out0##__LINE__, __out1##__LINE__, __out2##__LINE__, __out3##__LINE__, __out4##__LINE__, __out5##__LINE__, __out6##__LINE__, __out7##__LINE__, \
          __tmp0##__LINE__, __tmp1##__LINE__, __tmp2##__LINE__, __tmp3##__LINE__, __tmp4##__LINE__, __tmp5##__LINE__, __tmp6##__LINE__, __tmp7##__LINE__, \
          __tmpp0##__LINE__, __tmpp1##__LINE__, __tmpp2##__LINE__, __tmpp3##__LINE__, __tmpp4##__LINE__, __tmpp5##__LINE__, __tmpp6##__LINE__, __tmpp7##__LINE__)

#endif //__AVX512F__
