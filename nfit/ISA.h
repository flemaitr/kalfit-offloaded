#pragma once

namespace ISAs {
  struct DEFAULT {
    template <class F>
    static constexpr std::size_t card() { return 1; }
  };
  struct SCALAR {
    template <class F>
    static constexpr std::size_t card() { return 1; }
  };
  struct SSE {
    template <class F>
    static constexpr std::size_t card() { return 16 / sizeof(F); }
  };
  struct AVX {
    template <class F>
    static constexpr std::size_t card() { return 32 / sizeof(F); }
  };
  struct AVX512 {
    template <class F>
    static constexpr std::size_t card() { return 64 / sizeof(F); }
  };

#if defined(__AVX512F__)
  using CURRENT = AVX512;
#elif defined(__AVX__)
  using CURRENT = AVX;
#elif defined(__SSE__)
  using CURRENT = SSE;
#else
  using CURRENT = SCALAR;
#endif
}
