#pragma once

#include <vector>
#include <functional>
#include "../oldfit/MatrixTypes.h"
#include "../oldfit/Types.h"
#include "nStore.h"
#include "c++14_compat.h"

template <unsigned W> struct Vectype {};
template<> struct Vectype<2> { using type = Vec2d; using booltype = Vec2db; using inttype = Vec2q; };

#if INSTRSET >= 7
template<> struct Vectype<4> { using type = Vec4d; using booltype = Vec4db; using inttype = Vec4q; };
#endif
#if INSTRSET >= 9
template<> struct Vectype<8> { using type = Vec8d; using booltype = Vec8db; using inttype = Vec8q; };
#endif

#ifdef __INTEL_COMPILER
typedef double * const __restrict__ __attribute__((align_value(64))) double_ptr_64;
typedef const double * const __restrict__ __attribute__((align_value(64))) double_ptr_64_const;
#else
typedef double * const __restrict__ _aligned double_ptr_64;
typedef const double * const __restrict__ _aligned double_ptr_64_const;
#endif

// Names for templates
namespace Fit {
  class Backward;
  class Forward;
  class Predicted;
  class Updated;
  class Smooth;
  class StateVector;
  class Covariance;
}

struct nTrackVector {
  double* basePointer = 0x0;
  nTrackVector () = default;
  nTrackVector (double* basePointer) : basePointer(basePointer) {}
  /**
   * @brief      Copies v into its state
   *             Assumes basePointer is well defined
   */
  inline void copy (const nTrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void copy (const TrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void operator= (const TrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void operator+= (const TrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) += v[i];
    }
  }
  // Request item i
  inline double& operator[] (const unsigned int i) { return basePointer[i * VECTOR_WIDTH]; }
  inline double operator[] (const unsigned int i) const { return basePointer[i * VECTOR_WIDTH]; }
  inline void setBasePointer (const nTrackVector& v) { basePointer = v.basePointer; }
  inline void setBasePointer (double* basePointer) { this->basePointer = basePointer; }
};

struct nTrackSymMatrix {
  double* basePointer = 0x0;
  nTrackSymMatrix () = default;
  nTrackSymMatrix (double* basePointer) : basePointer(basePointer) {}
  /**
   * @brief      Copies v into its state
   *             Assumes basePointer is well defined
   */
  inline void copy (const nTrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void copy (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void operator= (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void operator+= (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) += v[i];
    }
  }
  // Request item i
  inline double& operator[] (const unsigned int i) { return basePointer[i * VECTOR_WIDTH]; }
  inline double operator[] (const unsigned int i) const { return basePointer[i * VECTOR_WIDTH]; }
  inline double& operator()(const int row, const int col) {
    return row>col ?
      basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }
  inline double operator()(const int row, const int col) const {
    return row>col ?
      basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }
  inline void setBasePointer (const nTrackSymMatrix& m) { basePointer = m.basePointer; }
  inline void setBasePointer (double* basePointer) { this->basePointer = basePointer; }
};

struct SmoothState {
  double* basePointer = 0x0;
  nTrackVector m_state;
  nTrackSymMatrix m_covariance;
  double* m_residual;
  double* m_errResidual;

  SmoothState () = default;
  SmoothState (double* basePointer) {
    setBasePointer(basePointer);
  }

  void setBasePointer (double* p) {
    basePointer = p;
    m_state = nTrackVector(p); p += 5 * VECTOR_WIDTH;
    m_covariance = nTrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_residual = p; p += VECTOR_WIDTH;
    m_errResidual = p;
  }

  void setBasePointer (const SmoothState& s) {
    setBasePointer(s.basePointer);
  }

  void copy (const SmoothState& s) {
    m_state.copy(s.m_state);
    m_covariance.copy(s.m_covariance);
    *m_residual = *s.m_residual;
    *m_errResidual = *s.m_errResidual;
  }
};

/**
 * @brief      Our flamant AOSOA
 */
struct nStates {
  double* basePointer = 0x0;
  nTrackVector m_predictedState;
  nTrackVector m_updatedState;
  nTrackSymMatrix m_predictedCovariance;
  nTrackSymMatrix m_updatedCovariance;
  double* m_chi2;

  nStates () = default;
  nStates (double* basePointer) {
    setBasePointer(basePointer);
  }

  void setBasePointer (double* p) {
    basePointer = p;
    m_predictedState = nTrackVector(p); p += 5 * VECTOR_WIDTH;
    m_updatedState   = nTrackVector(p); p += 5 * VECTOR_WIDTH;
    m_predictedCovariance = nTrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_updatedCovariance   = nTrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_chi2 = p;
  }

  void setBasePointer (const nStates& s) {
    setBasePointer(s.basePointer);
  }

  void copy (const nStates& s) {
    m_predictedState.copy(s.m_predictedState);
    m_updatedState.copy(s.m_updatedState);
    m_predictedCovariance.copy(s.m_predictedCovariance);
    m_updatedCovariance.copy(s.m_updatedCovariance);
    *m_chi2 = *(s.m_chi2);
  }

  nStates operator++ () {
    setBasePointer(basePointer + 41 * VECTOR_WIDTH);

    return *this;
  }

  nStates operator-- () {
    setBasePointer(basePointer - 41 * VECTOR_WIDTH);

    return *this;
  }
};

struct nStateVector {
  TrackVector m_parameters _aligned;
  double      m_z _aligned;

  nStateVector () = default;
  nStateVector (const StateVector& state);
};

struct nFitParameters {
  nStates m_states;
  TrackMatrix m_transportMatrix _aligned;
  bool m_hasInfoUpstream;

  nFitParameters (const nFitParameters& node) = default;
  nFitParameters (const FitNode& node, const int& direction);
};

struct nNode {
  Type m_type _aligned;
  nStateVector m_refVector _aligned;
  bool m_refIsSet _aligned;
  int* m_measurement _aligned;
  double m_errMeasure _aligned;
  TrackProjectionMatrix m_projectionMatrix _aligned;

  nNode () = default;
  nNode (const nNode& node) = default;
  nNode (const FitNode& node);
};

struct nFitNode : public nNode {
  TrackVector m_transportVector _aligned;
  TrackSymMatrix m_noiseMatrix _aligned;
  nFitParameters m_forwardFit _aligned;
  nFitParameters m_backwardFit _aligned;
  SmoothState m_smoothState _aligned;
  double m_refResidual _aligned;
  bool m_isLast _aligned = false;
  
  // Parameters to allow the conversion back
  // Eventually will be removed
  int m_parent_nTrackParameters;
  CachedBool m_hasInfoUpstream [2];
  int m_index;

  nFitNode () = default;
  nFitNode (const nFitNode& node) = default;
  nFitNode (const FitNode& node);
  nFitNode (const FitNode& node, const int index);

  template<class R, class S, class T,
    class U = typename std::conditional<std::is_same<T, Fit::Covariance>::value, nTrackSymMatrix, nTrackVector>::type>
  inline const U& getState () const;
  template<class R, class S, class T,
    class U = typename std::conditional<std::is_same<T, Fit::Covariance>::value, nTrackSymMatrix, nTrackVector>::type>
  inline U& getState ();

  template<class R,
    class U = typename std::conditional<std::is_same<R, Fit::Covariance>::value, nTrackSymMatrix, nTrackVector>::type>
  const U& getSmooth () const;
  template<class R,
    class U = typename std::conditional<std::is_same<R, Fit::Covariance>::value, nTrackSymMatrix, nTrackVector>::type>
  U& getSmooth ();

  template<class R>
  inline const nFitParameters& getFit () const;
  template<class R>
  inline nFitParameters& getFit ();

  template<class R>
  inline const double& getChi2 () const {
    return *(getFit<R>().m_states.m_chi2);
  }
  template<class R>
  inline double& getChi2 () {
    return *(getFit<R>().m_states.m_chi2);
  }
};

struct nTrack {
  std::vector<nFitNode> m_nodes _aligned;

  // Active measurements from this node
  unsigned int m_forwardUpstream _aligned;
  unsigned int m_backwardUpstream _aligned;

  // Chi square of track
  double m_forwardFitChi2 _aligned = 0.0;
  double m_backwardFitChi2 _aligned = 0.0;
  int m_ndof = 0;
  int m_index;

  // Outlier
  int m_outlier = -1;

  // Initial covariances
  TrackSymMatrix m_initialForwardCovariance;
  TrackSymMatrix m_initialBackwardCovariance;

  nTrack () = default;
  nTrack (const nTrack& track) = default;
  nTrack (const std::vector<FitNode>& track, int trackNumber, GrowingMemManager& memManager);
};

#include "nTypes_getters.h"
