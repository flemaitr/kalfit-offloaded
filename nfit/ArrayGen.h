#pragma once

#ifdef __SSE__
#include "immintrin.h"
#endif
#include "nTypes.h"
#include "Scheduler.h"
#include "ISA.h"
#include "simd-transpose.h"

// ------------------
// Array constructors
// ------------------

namespace {
  template <class T>
  static constexpr T round_down(T n, T align) {
    return n & -align;
  }
  template <class T>
  static constexpr T round_up(T n, T align) {
    return (n + align-1) & -align;
  }
  template <std::size_t N, class ISA, class T>
  struct transpose_helper_soaize {
    template <std::size_t W, class F, class G>
    static void soaize(F in_rows, G out_rows) {
      for (int i = 0; i < N; i++) {
        for (int j = 0; j < W; j++) {
          out_rows(i)[j] = in_rows(j)[i];
        }
      }
    }
  };
  template <std::size_t N, class ISA, class T>
  struct transpose_helper_get {
    template <class F, std::size_t... Is>
    static std::array<T, N*sizeof...(Is)> get(
      F getter,
      std::index_sequence<Is...>
    ) {
      constexpr std::size_t W = sizeof...(Is);
      std::array<T, N*W> soa;
      transpose_helper_soaize<N, ISA, T>::template soaize<W>(
              getter,
              [&soa](int i){ return &soa[i*W]; }
      );
      return soa;
    }
  };

  template <std::size_t N, class ISA = ISAs::CURRENT, class T = double>
  struct transpose_helper {
    template <std::size_t W, class F>
    static std::array<T, N*W> get(
      F getter
    ) {
      return transpose_helper_get<N, ISA, T>::get(getter, std::make_index_sequence<W>{});
    }
  };

  template <class T>
  struct transpose_helper_get<5, ISAs::SCALAR, T> {
    template <class F, std::size_t... Is>
    static std::array<T, 5*sizeof...(Is)> get(
      F getter,
      std::index_sequence<Is...>
    ) {
      constexpr std::size_t W = sizeof...(Is);
      return std::array<double, 5*sizeof...(Is)> {
        getter(Is)[0]... ,
        getter(Is)[1]... ,
        getter(Is)[2]... ,
        getter(Is)[3]... ,
        getter(Is)[4]...
      };
    }
  };

  template <class T>
  struct transpose_helper_get<15, ISAs::SCALAR, T> {
    template <class F, std::size_t... Is>
    static std::array<T, 15*sizeof...(Is)> get(
      F getter,
      std::index_sequence<Is...>
    ) {
      constexpr std::size_t W = sizeof...(Is);
      return std::array<double, 15*sizeof...(Is)> {
        getter(Is)[0]... ,
        getter(Is)[1]... ,
        getter(Is)[2]... ,
        getter(Is)[3]... ,
        getter(Is)[4]... ,
        getter(Is)[5]... ,
        getter(Is)[6]... ,
        getter(Is)[7]... ,
        getter(Is)[8]... ,
        getter(Is)[9]... ,
        getter(Is)[10]... ,
        getter(Is)[11]... ,
        getter(Is)[12]... ,
        getter(Is)[13]... ,
        getter(Is)[14]...
      };
    }
  };

  template <class T>
  struct transpose_helper_get<25, ISAs::SCALAR, T> {
    template <class F, std::size_t... Is>
    static std::array<T, 25*sizeof...(Is)> get(
      F getter,
      std::index_sequence<Is...>
    ) {
      constexpr std::size_t W = sizeof...(Is);
      return std::array<double, 25*sizeof...(Is)> {
        getter(Is)[0]... ,
        getter(Is)[1]... ,
        getter(Is)[2]... ,
        getter(Is)[3]... ,
        getter(Is)[4]... ,
        getter(Is)[5]... ,
        getter(Is)[6]... ,
        getter(Is)[7]... ,
        getter(Is)[8]... ,
        getter(Is)[9]... ,
        getter(Is)[10]... ,
        getter(Is)[11]... ,
        getter(Is)[12]... ,
        getter(Is)[13]... ,
        getter(Is)[14]... ,
        getter(Is)[15]... ,
        getter(Is)[16]... ,
        getter(Is)[17]... ,
        getter(Is)[18]... ,
        getter(Is)[19]... ,
        getter(Is)[20]... ,
        getter(Is)[21]... ,
        getter(Is)[22]... ,
        getter(Is)[23]... ,
        getter(Is)[24]...
      };
    }
  };

#ifdef __SSE__
  template <std::size_t N>
  struct transpose_helper_soaize<N, ISAs::SSE, double> {
    template <std::size_t W, class F, class G>
    static void soaize(F in_rows, G out_rows) {
      constexpr std::size_t sse_card = ISAs::SSE::card<double>();
      int i, j;
      static_assert(W % sse_card == 0, "Width is not a multiple of SIMD card (2)");
      for (i = 0; i < round_down(N, sse_card); i += sse_card) {
        for (j = 0; j < W; j += sse_card) {
          __m128d r0, r1;
          r0 = _mm_load_pd(&in_rows(j+0)[i]);
          r1 = _mm_load_pd(&in_rows(j+1)[i]);

          _MM_TRANSPOSE2_PD(r0, r1);

          _mm_store_pd(&out_rows(i+0)[j], r0);
          _mm_store_pd(&out_rows(i+1)[j], r1);
        }
      } /* remainder */ {
        i = round_down(N, sse_card);
        if (i == N-1) {
          for (j = 0; j < W; j += sse_card) {
            __m128d r0, r1;
            r0 = _mm_load_pd(&in_rows(j+0)[i]);
            r1 = _mm_load_pd(&in_rows(j+1)[i]);

            _MM_TRANSPOSE2_PD(r0, r1);

            _mm_store_pd(&out_rows(i+0)[j], r0);
          }
        }
      }
    }
  };
#endif //__SSE__

#ifdef __AVX__
  template <std::size_t N>
  struct transpose_helper_soaize<N, ISAs::AVX, double> {
    template <std::size_t W, class F, class G>
    static void soaize(F in_rows, G out_rows) {
      constexpr std::size_t sse_card = ISAs::SSE::card<double>();
      constexpr std::size_t avx_card = ISAs::AVX::card<double>();
      int i, j;
      static_assert(W % avx_card == 0, "Width is not a multiple of SIMD card (4)");
      for (i = 0; i < round_down(N, avx_card); i += avx_card) {
        for (j = 0; j < W; j += avx_card) {
          __m256d r0, r1, r2, r3;
          r0 = _mm256_loadu_pd(&in_rows(j+0)[i]);
          r1 = _mm256_loadu_pd(&in_rows(j+1)[i]);
          r2 = _mm256_loadu_pd(&in_rows(j+2)[i]);
          r3 = _mm256_loadu_pd(&in_rows(j+3)[i]);

          _MM256_TRANSPOSE4_PD(r0, r1, r2, r3);

          _mm256_store_pd(&out_rows(i+0)[j], r0);
          _mm256_store_pd(&out_rows(i+1)[j], r1);
          _mm256_store_pd(&out_rows(i+2)[j], r2);
          _mm256_store_pd(&out_rows(i+3)[j], r3);
        }
      } /* remainder */ {
        i = round_down(N, avx_card);
        for (j = 0; j < W; j += avx_card) {
          __m256d r0, r1, r2, r3;
          /*if (N - i == 1) {
            r0 = _mm256_castpd128_pd256(_mm_load_sd(&in_rows(j+0)[i]));
            r1 = _mm256_castpd128_pd256(_mm_load_sd(&in_rows(j+1)[i]));
            r2 = _mm256_castpd128_pd256(_mm_load_sd(&in_rows(j+2)[i]));
            r3 = _mm256_castpd128_pd256(_mm_load_sd(&in_rows(j+3)[i]));
          } else*/ if (N - i <= 2) {
            r0 = _mm256_castpd128_pd256(_mm_load_pd(&in_rows(j+0)[i]));
            r1 = _mm256_castpd128_pd256(_mm_load_pd(&in_rows(j+1)[i]));
            r2 = _mm256_castpd128_pd256(_mm_load_pd(&in_rows(j+2)[i]));
            r3 = _mm256_castpd128_pd256(_mm_load_pd(&in_rows(j+3)[i]));
          } else {
            r0 = _mm256_loadu_pd(&in_rows(j+0)[i]);
            r1 = _mm256_loadu_pd(&in_rows(j+1)[i]);
            r2 = _mm256_loadu_pd(&in_rows(j+2)[i]);
            r3 = _mm256_loadu_pd(&in_rows(j+3)[i]);
          }

          _MM256_TRANSPOSE4_PD(r0, r1, r2, r3);

          switch (N - i) {
          case 4:
            _mm256_store_pd(&out_rows(i+3)[j], r3);
          case 3:
            _mm256_store_pd(&out_rows(i+2)[j], r2);
          case 2:
            _mm256_store_pd(&out_rows(i+1)[j], r1);
          case 1:
            _mm256_store_pd(&out_rows(i+0)[j], r0);
          }
        }
      }
    }
  };
#endif //__AVX__

#ifdef __AVX512F__
  template <std::size_t N>
  struct transpose_helper_soaize<N, ISAs::AVX512, double> {
    template <std::size_t W, class F, class G>
    static void soaize(F in_rows, G out_rows) {
      //constexpr std::size_t N = 15;
      constexpr std::size_t sse_card = ISAs::SSE::card<double>();
      constexpr std::size_t avx_card = ISAs::AVX::card<double>();
      constexpr std::size_t avx512_card = ISAs::AVX512::card<double>();
      int i, j;
      static_assert(W % avx512_card == 0, "Width is not a multiple of SIMD card (8)");
      for (i = 0; i < round_down(N, avx512_card); i += avx512_card) {
        for (j = 0; j < W; j += avx512_card) {
          __m512d r0, r1, r2, r3, r4, r5, r6, r7;
          r0 = _mm512_loadu_pd(&in_rows(j+0)[i]);
          r1 = _mm512_loadu_pd(&in_rows(j+1)[i]);
          r2 = _mm512_loadu_pd(&in_rows(j+2)[i]);
          r3 = _mm512_loadu_pd(&in_rows(j+3)[i]);
          r4 = _mm512_loadu_pd(&in_rows(j+4)[i]);
          r5 = _mm512_loadu_pd(&in_rows(j+5)[i]);
          r6 = _mm512_loadu_pd(&in_rows(j+6)[i]);
          r7 = _mm512_loadu_pd(&in_rows(j+7)[i]);

          _MM512_TRANSPOSE8_PD(r0, r1, r2, r3, r4, r5, r6, r7);

          _mm512_store_pd(&out_rows(i+0)[j], r0);
          _mm512_store_pd(&out_rows(i+1)[j], r1);
          _mm512_store_pd(&out_rows(i+2)[j], r2);
          _mm512_store_pd(&out_rows(i+3)[j], r3);
          _mm512_store_pd(&out_rows(i+4)[j], r4);
          _mm512_store_pd(&out_rows(i+5)[j], r5);
          _mm512_store_pd(&out_rows(i+6)[j], r6);
          _mm512_store_pd(&out_rows(i+7)[j], r7);
        }
      } /* remainder */ {
        i = round_down(N, avx512_card);

        for (j = 0; j < W; j += avx512_card) {
          __m512d r0, r1, r2, r3, r4, r5, r6, r7;
          if (N - i == 1) {
            r0 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+0)[i]));
            r1 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+1)[i]));
            r2 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+2)[i]));
            r3 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+3)[i]));
            r4 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+4)[i]));
            r5 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+5)[i]));
            r6 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+6)[i]));
            r7 = _mm512_castpd128_pd512(_mm_load_sd(&in_rows(j+7)[i]));
          } else if (N - i <= 2) {
            r0 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+0)[i]));
            r1 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+1)[i]));
            r2 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+2)[i]));
            r3 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+3)[i]));
            r4 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+4)[i]));
            r5 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+5)[i]));
            r6 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+6)[i]));
            r7 = _mm512_castpd128_pd512(_mm_load_pd(&in_rows(j+7)[i]));
          } else if (N - i <= 4) {
            r0 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+0)[i]));
            r1 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+1)[i]));
            r2 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+2)[i]));
            r3 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+3)[i]));
            r4 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+4)[i]));
            r5 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+5)[i]));
            r6 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+6)[i]));
            r7 = _mm512_castpd256_pd512(_mm256_loadu_pd(&in_rows(j+7)[i]));
          } else {
            r0 = _mm512_loadu_pd(&in_rows(j+0)[i]);
            r1 = _mm512_loadu_pd(&in_rows(j+1)[i]);
            r2 = _mm512_loadu_pd(&in_rows(j+2)[i]);
            r3 = _mm512_loadu_pd(&in_rows(j+3)[i]);
            r4 = _mm512_loadu_pd(&in_rows(j+4)[i]);
            r5 = _mm512_loadu_pd(&in_rows(j+5)[i]);
            r6 = _mm512_loadu_pd(&in_rows(j+6)[i]);
            r7 = _mm512_loadu_pd(&in_rows(j+7)[i]);
          }

          _MM512_TRANSPOSE8_PD(r0, r1, r2, r3, r4, r5, r6, r7);

          switch (N - i) {
          case 8:
            _mm512_store_pd(&out_rows(i+7)[j], r7);
          case 7:
            _mm512_store_pd(&out_rows(i+6)[j], r6);
          case 6:
            _mm512_store_pd(&out_rows(i+5)[j], r5);
          case 5:
            _mm512_store_pd(&out_rows(i+4)[j], r4);
          case 4:
            _mm512_store_pd(&out_rows(i+3)[j], r3);
          case 3:
            _mm512_store_pd(&out_rows(i+2)[j], r2);
          case 2:
            _mm512_store_pd(&out_rows(i+1)[j], r1);
          case 1:
            _mm512_store_pd(&out_rows(i+0)[j], r0);
          }
        }
      }
    }
  };
#endif //__AVX512F__
}

struct ArrayGen {
  template<size_t W>
  static inline constexpr uint8_t mask () {
    return (W==8 ? 0xFF : (W==4 ? 0x0F : 0x03));
  }
  
  template<size_t W>
  static inline constexpr std::array<double, 5*W> getReferenceVector (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return nodes[j].node->m_refVector.m_parameters.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 5*W> getProjectionMatrix (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return nodes[j].node->m_projectionMatrix.fArray; });
  }

  // Simple values are trivially soaized
  template<size_t W>
  static inline constexpr std::array<double, W> getRefResidual (
    std::array<SchItem, W>& nodes
  ) {
    return getRefResidual(nodes, std::make_index_sequence<W>{});
  }
  template<size_t... Is>
  static inline constexpr std::array<double, sizeof...(Is)> getRefResidual (
    const std::array<SchItem, sizeof...(Is)>& nodes,
    const std::index_sequence<Is...>
  ) {
    return std::array<double, sizeof...(Is)> {
      nodes[Is].node->m_refResidual...
    };
  }

  template<size_t W>
  static inline constexpr std::array<double, W> getErrMeasure2 (
    std::array<SchItem, W>& nodes
  ) {
    return getErrMeasure2(nodes, std::make_index_sequence<W>{});
  }
  template<size_t... Is>
  static inline constexpr std::array<double, sizeof...(Is)> getErrMeasure2 (
    const std::array<SchItem, sizeof...(Is)>& nodes,
    const std::index_sequence<Is...>
  ) {
    return std::array<double, sizeof...(Is)> {
      nodes[Is].node->m_errMeasure * nodes[Is].node->m_errMeasure...
    };
  }

  template<size_t W>
  static inline constexpr std::array<double, 5*W> getCurrentProjectionMatrix (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return nodes[j].node->m_projectionMatrix.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 5*W> getCurrentParameters (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return nodes[j].node->m_refVector.m_parameters.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 5*W> getPreviousTransportVector (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return nodes[j].prevnode->m_transportVector.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 5*W> getCurrentTransportVector (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return nodes[j].node->m_transportVector.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 15*W> getPreviousNoiseMatrix (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<15>::get<W>([&nodes](int j){ return nodes[j].prevnode->m_noiseMatrix.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 15*W> getCurrentNoiseMatrix (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<15>::get<W>([&nodes](int j){ return nodes[j].node->m_noiseMatrix.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 25*W> getPreviousTransportMatrix (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<25>::get<W>([&nodes](int j){ return nodes[j].prevnode->m_backwardFit.m_transportMatrix.fArray; });
  }

  template<size_t W>
  static inline constexpr std::array<double, 25*W> getCurrentTransportMatrix (
    std::array<SchItem, W>& nodes
  ) {
    return transpose_helper<25>::get<W>([&nodes](int j){ return nodes[j].node->m_forwardFit.m_transportMatrix.fArray; });
  }
};
