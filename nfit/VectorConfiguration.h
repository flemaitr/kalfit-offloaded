#pragma once

#define _aligned __attribute__((aligned(64)))

// Include vectorclass
#define MAX_VECTOR_SIZE 512
#include "../vectorclass/vectorclass.h"

#define TO_STRING(s) WRAPPED_TO_STRING(s)
#define WRAPPED_TO_STRING(s) #s

// Allow for static definition
// #define STATIC_VECTOR_WIDTH 4

// Set compiling values
#ifdef STATIC_VECTOR_WIDTH
  #define VECTOR_WIDTH STATIC_VECTOR_WIDTH
#else
#if INSTRSET < 7
  #define VECTOR_WIDTH 2
#elif INSTRSET < 9
  #define VECTOR_WIDTH 4
#else
  #define VECTOR_WIDTH 8
#endif
#endif
