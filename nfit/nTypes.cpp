#include "nTypes.h"

nStateVector::nStateVector (const StateVector& state) {
  m_parameters = state.m_parameters;
  m_z = state.m_z;
}

nFitParameters::nFitParameters (const FitNode& node, const int& direction) {
  // m_predictedState = node.m_predictedState[direction];
  // m_updatedState = node.m_filteredState[direction];
  m_transportMatrix = (direction==Forward ? node.m_transportMatrix : node.m_invertTransportMatrix);
  // TODO
  // m_chi2 = node.m_deltaChi2[direction].m_chi2;
  m_hasInfoUpstream = (node.m_hasInfoUpstream[direction] == True ? true : false);
}

nNode::nNode (const FitNode& node) {
  m_type = node.m_type;
  m_refVector = node.m_refVector;
  m_refIsSet = node.m_refIsSet;
  m_measurement = node.m_measurement;
  m_errMeasure = node.m_errMeasure;
  m_projectionMatrix = node.m_projectionMatrix;
}

nFitNode::nFitNode (const FitNode& node, const int index)
  : nNode(node), m_forwardFit(node, Forward), m_backwardFit(node, Backward),
  m_noiseMatrix(node.m_noiseMatrix), m_transportVector(node.m_transportVector),
  m_refResidual(node.m_refResidual), m_parent_nTrackParameters(node.m_parent_nTrackParameters),
  m_index(index) {
  // Just to be able to translate it back
  m_hasInfoUpstream[0] = node.m_hasInfoUpstream[0];
  m_hasInfoUpstream[1] = node.m_hasInfoUpstream[1];
}

nFitNode::nFitNode (const FitNode& node)
  : nNode(node), m_forwardFit(node, Forward), m_backwardFit(node, Backward),
  m_noiseMatrix(node.m_noiseMatrix), m_transportVector(node.m_transportVector),
  m_refResidual(node.m_refResidual), m_parent_nTrackParameters(node.m_parent_nTrackParameters) {
  // Just to be able to translate it back
  m_hasInfoUpstream[0] = node.m_hasInfoUpstream[0];
  m_hasInfoUpstream[1] = node.m_hasInfoUpstream[1];
}

nTrack::nTrack (
  const std::vector<FitNode>& track,
  int trackNumber,
  GrowingMemManager& memManager
) : m_index(trackNumber) {
  // Generate the nodes
  int i=0;
  for (const auto& node : track) {
    m_nodes.push_back(nFitNode(node, i++));
  }
  m_nodes.back().m_isLast = true;

  // Copy the initial forward predicted and backward predicted covariance
  m_initialForwardCovariance  = track.front().m_predictedState[0].m_covariance;
  m_initialBackwardCovariance = track.back().m_predictedState[1].m_covariance;

  // Copy all other state and covariances (to avoid 0x0 accesses - this shouldn't be in Gaudi)
  for (int i=0; i<m_nodes.size(); ++i) {
    const FitNode& oldnode = track[i];
    nFitNode& node = m_nodes[i];

    node.getFit<Fit::Forward>().m_states.setBasePointer(nStates(memManager.getNextElement()));
    node.getFit<Fit::Backward>().m_states.setBasePointer(nStates(memManager.getNextElement()));

    node.getState<Fit::Forward, Fit::Predicted, Fit::StateVector>() = oldnode.m_predictedState[0].m_stateVector;
    node.getState<Fit::Forward, Fit::Predicted, Fit::Covariance>() = oldnode.m_predictedState[0].m_covariance;
    node.getState<Fit::Backward, Fit::Predicted, Fit::StateVector>() = oldnode.m_predictedState[1].m_stateVector;
    node.getState<Fit::Backward, Fit::Predicted, Fit::Covariance>() = oldnode.m_predictedState[1].m_covariance;

    node.getState<Fit::Forward, Fit::Updated, Fit::StateVector>() = oldnode.m_filteredState[0].m_stateVector;
    node.getState<Fit::Forward, Fit::Updated, Fit::Covariance>() = oldnode.m_filteredState[0].m_covariance;
    node.getState<Fit::Backward, Fit::Updated, Fit::StateVector>() = oldnode.m_filteredState[1].m_stateVector;
    node.getState<Fit::Backward, Fit::Updated, Fit::Covariance>() = oldnode.m_filteredState[1].m_covariance;
  }

  m_forwardUpstream = 0;
  m_backwardUpstream = track.size() - 1;

  bool foundForward = false;
  bool foundBackward = false;
  for (int i=0; i<track.size(); ++i) {
    const int reverse_i = track.size() - i - 1;

    if (!foundForward && track[i].m_type == HitOnTrack) {
      foundForward = true;
      m_forwardUpstream = i;
    }

    if (!foundBackward && track[reverse_i].m_type == HitOnTrack) {
      foundBackward = true;
      m_backwardUpstream = i;
    }
  }
}
