// Getters for states

// ---------------
// get methods
// ---------------

template<>
inline const nFitParameters& nFitNode::getFit<Fit::Backward> () const {
  return m_backwardFit;
}

template<>
inline const nFitParameters& nFitNode::getFit<Fit::Forward> () const {
  return m_forwardFit;
}

template<>
inline const nTrackVector& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::StateVector> () const {
  return m_forwardFit.m_states.m_predictedState;
}

template<>
inline const nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::Covariance> () const {
  return m_forwardFit.m_states.m_predictedCovariance;
}

template<>
inline const nTrackVector& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::StateVector> () const {
  return m_forwardFit.m_states.m_updatedState;
}

template<>
inline const nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::Covariance> () const {
  return m_forwardFit.m_states.m_updatedCovariance;
}

template<>
inline const nTrackVector& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::StateVector> () const {
  return m_backwardFit.m_states.m_predictedState;
}

template<>
inline const nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::Covariance> () const {
  return m_backwardFit.m_states.m_predictedCovariance;
}

template<>
inline const nTrackVector& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::StateVector> () const {
  return m_backwardFit.m_states.m_updatedState;
}

template<>
inline const nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::Covariance> () const {
  return m_backwardFit.m_states.m_updatedCovariance;
}

template<>
inline const nTrackVector& nFitNode::getSmooth<Fit::StateVector> () const {
  return m_smoothState.m_state;
}

template<>
inline const nTrackSymMatrix& nFitNode::getSmooth<Fit::Covariance> () const {
  return m_smoothState.m_covariance;
}

// -------------------------
// ref getters for the above
// -------------------------

template<>
inline nFitParameters& nFitNode::getFit<Fit::Forward> () {
  return m_forwardFit;
}

template<>
inline nFitParameters& nFitNode::getFit<Fit::Backward> () {
  return m_backwardFit;
}

template<>
inline nTrackVector& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::StateVector> () {
  return m_forwardFit.m_states.m_predictedState;
}

template<>
inline nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::Covariance> () {
  return m_forwardFit.m_states.m_predictedCovariance;
}

template<>
inline nTrackVector& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::StateVector> () {
  return m_forwardFit.m_states.m_updatedState;
}

template<>
inline nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::Covariance> () {
  return m_forwardFit.m_states.m_updatedCovariance;
}

template<>
inline nTrackVector& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::StateVector> () {
  return m_backwardFit.m_states.m_predictedState;
}

template<>
inline nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::Covariance> () {
  return m_backwardFit.m_states.m_predictedCovariance;
}

template<>
inline nTrackVector& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::StateVector> () {
  return m_backwardFit.m_states.m_updatedState;
}

template<>
inline nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::Covariance> () {
  return m_backwardFit.m_states.m_updatedCovariance;
}

template<>
inline nTrackVector& nFitNode::getSmooth<Fit::StateVector> () {
  return m_smoothState.m_state;
}

template<>
inline nTrackSymMatrix& nFitNode::getSmooth<Fit::Covariance> () {
  return m_smoothState.m_covariance;
}