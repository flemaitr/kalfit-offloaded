#pragma once

#include "nTypes.h"
#include "../oldfit/Similarity.h"
#include "FitMath.h"
#include "ArrayGen.h"

template<class D>
void nupdate (
  nFitNode& node
) {
  if (node.m_type != HitOnTrack) {
    node.getState<D, Fit::Updated, Fit::StateVector>().setBasePointer(node.getState<D, Fit::Predicted, Fit::StateVector>());
    node.getState<D, Fit::Updated, Fit::Covariance>().setBasePointer(node.getState<D, Fit::Predicted, Fit::Covariance>());
  } else {
    node.getState<D, Fit::Updated, Fit::StateVector>().copy(node.getState<D, Fit::Predicted, Fit::StateVector>());
    node.getState<D, Fit::Updated, Fit::Covariance>().copy(node.getState<D, Fit::Predicted, Fit::Covariance>());

    math_update_nonvec (
      node.getState<D, Fit::Updated, Fit::StateVector>(),
      node.getState<D, Fit::Updated, Fit::Covariance>(),
      node.getChi2<D>(),
      node.m_refVector.m_parameters.fArray,
      node.m_projectionMatrix.fArray,
      node.m_refResidual,
      node.m_errMeasure * node.m_errMeasure
    );
  }
}

template<unsigned W>
void nupdate_vec (
  std::array<SchItem, W>& n,
  double_ptr_64_const ps,
  double_ptr_64_const pc,
  double_ptr_64 us,
  double_ptr_64 uc,
  double_ptr_64 chi2
) {
  const std::array<double, 5*W> _aligned Xref = ArrayGen::getReferenceVector(n);
  const std::array<double, 5*W> _aligned H = ArrayGen::getProjectionMatrix(n);
  const std::array<double, W> _aligned refResidual = ArrayGen::getRefResidual(n);
  const std::array<double, W> _aligned errorMeas2 = ArrayGen::getErrMeasure2(n);

  FitMathCommon<W>::update (
    us,
    uc,
    chi2,
    ps,
    pc,
    Xref,
    H,
    refResidual,
    errorMeas2,
    n
  );
}
