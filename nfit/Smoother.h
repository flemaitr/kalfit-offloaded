#pragma once

#include "nTypes.h"
#include "FitMath.h"
#include "../oldfit/Similarity.h"

template<bool F, bool B>
inline void smoother (
  nFitNode& node
);

template<>
inline void smoother<false, true> (
  nFitNode& node
) {
  node.getSmooth<Fit::StateVector>().setBasePointer(node.getState<Fit::Backward, Fit::Updated, Fit::StateVector>());
  node.getSmooth<Fit::Covariance>().setBasePointer(node.getState<Fit::Backward, Fit::Updated, Fit::Covariance>());
}

template<>
inline void smoother<true, false> (
  nFitNode& node
) {
  node.getSmooth<Fit::StateVector>().setBasePointer(node.getState<Fit::Forward, Fit::Updated, Fit::StateVector>());
  node.getSmooth<Fit::Covariance>().setBasePointer(node.getState<Fit::Forward, Fit::Updated, Fit::Covariance>());
}

template<>
inline void smoother<true, true> (
  nFitNode& node
) {
  math_average_nonvec (
    node.getState<Fit::Backward, Fit::Updated, Fit::StateVector>(),
    node.getState<Fit::Backward, Fit::Updated, Fit::Covariance>(),
    node.getState<Fit::Forward, Fit::Predicted, Fit::StateVector>(),
    node.getState<Fit::Forward, Fit::Predicted, Fit::Covariance>(),
    node.getSmooth<Fit::StateVector>(),
    node.getSmooth<Fit::Covariance>()
  );
}

void updateResiduals (
  nFitNode& node
);

template<long unsigned W>
inline uint8_t smoother_vec (
  double_ptr_64_const s1,
  double_ptr_64_const s2,
  double_ptr_64_const c1,
  double_ptr_64_const c2,
  double_ptr_64 ss,
  double_ptr_64 sc
) {
  return FitMathCommon<W>::average (
    s1,
    c1,
    s2,
    c2,
    ss,
    sc
  );
}

template<long unsigned W>
void updateResiduals_vec (
  std::array<SchItem, W>& n,
  double_ptr_64_const ss,
  double_ptr_64_const sc,
  double_ptr_64 res,
  double_ptr_64 errRes
) {
  const std::array<double, 5*W> _aligned pm = ArrayGen::getCurrentProjectionMatrix(n);
  const std::array<double, 5*W> _aligned pa = ArrayGen::getCurrentParameters(n);
  const std::array<double, W> _aligned em = ArrayGen::getErrMeasure2(n);
  const std::array<double, W> _aligned rr = ArrayGen::getRefResidual(n);

  FitMathCommon<W>::updateResiduals (
    n,
    pm,
    pa,
    em,
    rr,
    ss,
    sc,
    res,
    errRes
  );
}
