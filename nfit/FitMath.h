#pragma once

#include <array>
#include "nStore.h"
#include "nTypes.h"
#include "ArrayGen.h"

template<unsigned W>
struct FitMathCommon {
  static constexpr inline unsigned symid (const int row, const int col) {
    return ((row * (row + 1)) / 2) + col;
  }

  template <size_t... Is>
  static constexpr typename Vectype<W>::booltype makeBoolHitOnTrack (
    const std::array<SchItem, W>& _aligned nodes,
    std::index_sequence<Is...>
  ) {
    return typename Vectype<W>::booltype(nodes[Is].node->m_type == HitOnTrack...);
  }

  template <size_t... Is>
  static constexpr typename Vectype<W>::booltype makeBoolMeasurement (
    const std::array<SchItem, W>& _aligned nodes,
    std::index_sequence<Is...>
  ) {
    return typename Vectype<W>::booltype(nodes[Is].node->m_measurement != nullptr...);
  }

  /**
   * @brief      Similarity transform, manually horizontally vectorised
   *
   * @param[in]  tm  Transport matrix
   * @param[in]  uc  Previous update covariance
   * @param      pc  Predicted covariance
   */
  static void similarity_5_5 (
    const std::array<double, 25*W>& _aligned tm,
    double_ptr_64_const uc,
    double_ptr_64 pc
  ) {
    using vectype = typename Vectype<W>::type;
    vectype tm0, tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13, tm14, tm15, tm16, tm17, tm18, tm19, tm20, tm21, tm22, tm23, tm24;
    vectype uc0, uc1, uc2, uc3, uc4, uc5, uc6, uc7, uc8, uc9, uc10, uc11, uc12, uc13, uc14;
    vectype pc0, pc1, pc2, pc3, pc4, pc5, pc6, pc7, pc8, pc9, pc10, pc11, pc12, pc13, pc14;
    vectype v0, v1, v2, v3, v4;

    // Load tm
    tm0.load_a(tm.data() + 0*W);
    tm1.load_a(tm.data() + 1*W);
    tm2.load_a(tm.data() + 2*W);
    tm3.load_a(tm.data() + 3*W);
    tm4.load_a(tm.data() + 4*W);
    tm5.load_a(tm.data() + 5*W);
    tm6.load_a(tm.data() + 6*W);
    tm7.load_a(tm.data() + 7*W);
    tm8.load_a(tm.data() + 8*W);
    tm9.load_a(tm.data() + 9*W);
    tm10.load_a(tm.data() + 10*W);
    tm11.load_a(tm.data() + 11*W);
    tm12.load_a(tm.data() + 12*W);
    tm13.load_a(tm.data() + 13*W);
    tm14.load_a(tm.data() + 14*W);
    tm15.load_a(tm.data() + 15*W);
    tm16.load_a(tm.data() + 16*W);
    tm17.load_a(tm.data() + 17*W);
    tm18.load_a(tm.data() + 18*W);
    tm19.load_a(tm.data() + 19*W);
    tm20.load_a(tm.data() + 20*W);
    tm21.load_a(tm.data() + 21*W);
    tm22.load_a(tm.data() + 22*W);
    tm23.load_a(tm.data() + 23*W);
    tm24.load_a(tm.data() + 24*W);

    // Load uc
    uc0.load_a(uc + 0*W);
    uc1.load_a(uc + 1*W);
    uc2.load_a(uc + 2*W);
    uc3.load_a(uc + 3*W);
    uc4.load_a(uc + 4*W);
    uc5.load_a(uc + 5*W);
    uc6.load_a(uc + 6*W);
    uc7.load_a(uc + 7*W);
    uc8.load_a(uc + 8*W);
    uc9.load_a(uc + 9*W);
    uc10.load_a(uc + 10*W);
    uc11.load_a(uc + 11*W);
    uc12.load_a(uc + 12*W);
    uc13.load_a(uc + 13*W);
    uc14.load_a(uc + 14*W);

    // v[0] = uc[ 0]*tm[0]+uc[ 1]*tm[1]+uc[ 3]*tm[2]+uc[ 6]*tm[3]+uc[10]*tm[4];
    // v[1] = uc[ 1]*tm[0]+uc[ 2]*tm[1]+uc[ 4]*tm[2]+uc[ 7]*tm[3]+uc[11]*tm[4];
    // v[2] = uc[ 3]*tm[0]+uc[ 4]*tm[1]+uc[ 5]*tm[2]+uc[ 8]*tm[3]+uc[12]*tm[4];
    // v[3] = uc[ 6]*tm[0]+uc[ 7]*tm[1]+uc[ 8]*tm[2]+uc[ 9]*tm[3]+uc[13]*tm[4];
    // v[4] = uc[10]*tm[0]+uc[11]*tm[1]+uc[12]*tm[2]+uc[13]*tm[3]+uc[14]*tm[4];
    v0 = uc0 *tm0+uc1 *tm1+uc3 *tm2+uc6 *tm3+uc10*tm4;
    v1 = uc1 *tm0+uc2 *tm1+uc4 *tm2+uc7 *tm3+uc11*tm4;
    v2 = uc3 *tm0+uc4 *tm1+uc5 *tm2+uc8 *tm3+uc12*tm4;
    v3 = uc6 *tm0+uc7 *tm1+uc8 *tm2+uc9 *tm3+uc13*tm4;
    v4 = uc10*tm0+uc11*tm1+uc12*tm2+uc13*tm3+uc14*tm4;

    // pc[ 0] = tm[ 0]*v[0] + tm[ 1]*v[1] + tm[ 2]*v[2] + tm[ 3]*v[3] + tm[ 4]*v[4];
    // pc[ 1] = tm[ 5]*v[0] + tm[ 6]*v[1] + tm[ 7]*v[2] + tm[ 8]*v[3] + tm[ 9]*v[4];
    // pc[ 3] = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    // pc[ 6] = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    // pc[10] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];
    pc0  = tm0 *v0 + tm1 *v1 + tm2 *v2 + tm3 *v3 + tm4 *v4;
    pc1  = tm5 *v0 + tm6 *v1 + tm7 *v2 + tm8 *v3 + tm9 *v4;
    pc3  = tm10*v0 + tm11*v1 + tm12*v2 + tm13*v3 + tm14*v4;
    pc6  = tm15*v0 + tm16*v1 + tm17*v2 + tm18*v3 + tm19*v4;
    pc10 = tm20*v0 + tm21*v1 + tm22*v2 + tm23*v3 + tm24*v4;


    // v[0] =  uc[ 0]*tm[5]+uc[ 1]*tm[6]+uc[ 3]*tm[7]+uc[ 6]*tm[8]+uc[10]*tm[9];
    // v[1] =  uc[ 1]*tm[5]+uc[ 2]*tm[6]+uc[ 4]*tm[7]+uc[ 7]*tm[8]+uc[11]*tm[9];
    // v[2] =  uc[ 3]*tm[5]+uc[ 4]*tm[6]+uc[ 5]*tm[7]+uc[ 8]*tm[8]+uc[12]*tm[9];
    // v[3] =  uc[ 6]*tm[5]+uc[ 7]*tm[6]+uc[ 8]*tm[7]+uc[ 9]*tm[8]+uc[13]*tm[9];
    // v[4] =  uc[10]*tm[5]+uc[11]*tm[6]+uc[12]*tm[7]+uc[13]*tm[8]+uc[14]*tm[9];
    v0 =  uc0 *tm5+uc1 *tm6+uc3 *tm7+uc6 *tm8+uc10*tm9;
    v1 =  uc1 *tm5+uc2 *tm6+uc4 *tm7+uc7 *tm8+uc11*tm9;
    v2 =  uc3 *tm5+uc4 *tm6+uc5 *tm7+uc8 *tm8+uc12*tm9;
    v3 =  uc6 *tm5+uc7 *tm6+uc8 *tm7+uc9 *tm8+uc13*tm9;
    v4 =  uc10*tm5+uc11*tm6+uc12*tm7+uc13*tm8+uc14*tm9;

    // pc[2]  = tm[ 5]*v[0] + tm[ 6]*v[1] + tm[ 7]*v[2] + tm[ 8]*v[3] + tm[ 9]*v[4];
    // pc[4]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    // pc[7]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    // pc[11] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];
    pc2  = tm5 *v0 + tm6 *v1 + tm7 *v2 + tm8 *v3 + tm9 *v4;
    pc4  = tm10*v0 + tm11*v1 + tm12*v2 + tm13*v3 + tm14*v4;
    pc7  = tm15*v0 + tm16*v1 + tm17*v2 + tm18*v3 + tm19*v4;
    pc11 = tm20*v0 + tm21*v1 + tm22*v2 + tm23*v3 + tm24*v4;

    // v[0] = uc[ 0]*tm[10]+uc[ 1]*tm[11]+uc[ 3]*tm[12]+uc[ 6]*tm[13]+uc[10]*tm[14];
    // v[1] = uc[ 1]*tm[10]+uc[ 2]*tm[11]+uc[ 4]*tm[12]+uc[ 7]*tm[13]+uc[11]*tm[14];
    // v[2] = uc[ 3]*tm[10]+uc[ 4]*tm[11]+uc[ 5]*tm[12]+uc[ 8]*tm[13]+uc[12]*tm[14];
    // v[3] = uc[ 6]*tm[10]+uc[ 7]*tm[11]+uc[ 8]*tm[12]+uc[ 9]*tm[13]+uc[13]*tm[14];
    // v[4] = uc[10]*tm[10]+uc[11]*tm[11]+uc[12]*tm[12]+uc[13]*tm[13]+uc[14]*tm[14];
    v0 = uc0 *tm10+uc1 *tm11+uc3 *tm12+uc6 *tm13+uc10*tm14;
    v1 = uc1 *tm10+uc2 *tm11+uc4 *tm12+uc7 *tm13+uc11*tm14;
    v2 = uc3 *tm10+uc4 *tm11+uc5 *tm12+uc8 *tm13+uc12*tm14;
    v3 = uc6 *tm10+uc7 *tm11+uc8 *tm12+uc9 *tm13+uc13*tm14;
    v4 = uc10*tm10+uc11*tm11+uc12*tm12+uc13*tm13+uc14*tm14;

    // pc[5]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    // pc[8]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    // pc[12] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];
    pc5  = tm10*v0 + tm11*v1 + tm12*v2 + tm13*v3 + tm14*v4;
    pc8  = tm15*v0 + tm16*v1 + tm17*v2 + tm18*v3 + tm19*v4;
    pc12 = tm20*v0 + tm21*v1 + tm22*v2 + tm23*v3 + tm24*v4;

    // v[0] = uc[ 0]*tm[15]+uc[ 1]*tm[16]+uc[ 3]*tm[17]+uc[ 6]*tm[18]+uc[10]*tm[19];
    // v[1] = uc[ 1]*tm[15]+uc[ 2]*tm[16]+uc[ 4]*tm[17]+uc[ 7]*tm[18]+uc[11]*tm[19];
    // v[2] = uc[ 3]*tm[15]+uc[ 4]*tm[16]+uc[ 5]*tm[17]+uc[ 8]*tm[18]+uc[12]*tm[19];
    // v[3] = uc[ 6]*tm[15]+uc[ 7]*tm[16]+uc[ 8]*tm[17]+uc[ 9]*tm[18]+uc[13]*tm[19];
    // v[4] = uc[10]*tm[15]+uc[11]*tm[16]+uc[12]*tm[17]+uc[13]*tm[18]+uc[14]*tm[19];
    v0 = uc0 *tm15+uc1 *tm16+uc3 *tm17+uc6 *tm18+uc10*tm19;
    v1 = uc1 *tm15+uc2 *tm16+uc4 *tm17+uc7 *tm18+uc11*tm19;
    v2 = uc3 *tm15+uc4 *tm16+uc5 *tm17+uc8 *tm18+uc12*tm19;
    v3 = uc6 *tm15+uc7 *tm16+uc8 *tm17+uc9 *tm18+uc13*tm19;
    v4 = uc10*tm15+uc11*tm16+uc12*tm17+uc13*tm18+uc14*tm19;

    // pc[9]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    // pc[13] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];
    pc9  = tm15*v0 + tm16*v1 + tm17*v2 + tm18*v3 + tm19*v4;
    pc13 = tm20*v0 + tm21*v1 + tm22*v2 + tm23*v3 + tm24*v4;

    // v[0] = uc[ 0]*tm[20]+uc[1]*tm[21]+uc[3]*tm[22]+uc[6]*tm[23]+uc[10]*tm[24];
    // v[1] = uc[ 1]*tm[20]+uc[2]*tm[21]+uc[4]*tm[22]+uc[7]*tm[23]+uc[11]*tm[24];
    // v[2] = uc[ 3]*tm[20]+uc[4]*tm[21]+uc[5]*tm[22]+uc[8]*tm[23]+uc[12]*tm[24];
    // v[3] = uc[ 6]*tm[20]+uc[7]*tm[21]+uc[8]*tm[22]+uc[9]*tm[23]+uc[13]*tm[24];
    // v[4] = uc[10]*tm[20]+uc[11]*tm[21]+uc[12]*tm[22]+uc[13]*tm[23]+uc[14]*tm[24];
    v0 = uc0 *tm20+uc1*tm21+uc3*tm22+uc6*tm23+uc10*tm24;
    v1 = uc1 *tm20+uc2*tm21+uc4*tm22+uc7*tm23+uc11*tm24;
    v2 = uc3 *tm20+uc4*tm21+uc5*tm22+uc8*tm23+uc12*tm24;
    v3 = uc6 *tm20+uc7*tm21+uc8*tm22+uc9*tm23+uc13*tm24;
    v4 = uc10*tm20+uc11*tm21+uc12*tm22+uc13*tm23+uc14*tm24;

    // pc[14]= tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];
    pc14= tm20*v0 + tm21*v1 + tm22*v2 + tm23*v3 + tm24*v4;

    // Store pc
    pc0.store_a(pc + 0*W);
    pc1.store_a(pc + 1*W);
    pc2.store_a(pc + 2*W);
    pc3.store_a(pc + 3*W);
    pc4.store_a(pc + 4*W);
    pc5.store_a(pc + 5*W);
    pc6.store_a(pc + 6*W);
    pc7.store_a(pc + 7*W);
    pc8.store_a(pc + 8*W);
    pc9.store_a(pc + 9*W);
    pc10.store_a(pc + 10*W);
    pc11.store_a(pc + 11*W);
    pc12.store_a(pc + 12*W);
    pc13.store_a(pc + 13*W);
    pc14.store_a(pc + 14*W);
  }

  /**
   * @brief      Vectorised update
   *
   * @param      us          Updated state
   * @param      uc          Updated covariance
   * @param      chi2        Chi2
   * @param[in]  ps          Predicted state
   * @param[in]  pc          Predicted covariance
   * @param[in]  Xref        Reference parameters
   * @param[in]  H           Projection matrix
   * @param[in]  refResidual Residual of reference
   * @param[in]  errorMeas2  Measurement error squared
   * @param[in]  nodes       Nodes
   */
  static void update (
    double_ptr_64 us,
    double_ptr_64 uc,
    double_ptr_64 chi2,
    double_ptr_64_const ps,
    double_ptr_64_const pc,
    const std::array<double, 5*W>& _aligned Xref,
    const std::array<double, 5*W>& _aligned H,
    const std::array<double, W>& _aligned refResidual,
    const std::array<double, W>& _aligned errorMeas2,
    const std::array<SchItem, W>& _aligned nodes
  )  {
    using vectype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    vectype uc0, uc1, uc2, uc3, uc4, uc5, uc6, uc7, uc8, uc9, uc10, uc11, uc12, uc13, uc14;
    vectype pc0, pc1, pc2, pc3, pc4, pc5, pc6, pc7, pc8, pc9, pc10, pc11, pc12, pc13, pc14;
    vectype us0, us1, us2, us3, us4;
    vectype ps0, ps1, ps2, ps3, ps4;
    vectype Xref0, Xref1, Xref2, Xref3, Xref4;
    vectype H0, H1, H2, H3, H4;
    vectype cht0, cht1, cht2, cht3, cht4;
    vectype res, vrefResidual, verrorMeas2, vchi2;

    // Load pc
    uc0.load_a(pc + 0*W);
    uc1.load_a(pc + 1*W);
    uc2.load_a(pc + 2*W);
    uc3.load_a(pc + 3*W);
    uc4.load_a(pc + 4*W);
    uc5.load_a(pc + 5*W);
    uc6.load_a(pc + 6*W);
    uc7.load_a(pc + 7*W);
    uc8.load_a(pc + 8*W);
    uc9.load_a(pc + 9*W);
    uc10.load_a(pc + 10*W);
    uc11.load_a(pc + 11*W);
    uc12.load_a(pc + 12*W);
    uc13.load_a(pc + 13*W);
    uc14.load_a(pc + 14*W);

    // Load ps
    us0.load_a(ps + 0*W);
    us1.load_a(ps + 1*W);
    us2.load_a(ps + 2*W);
    us3.load_a(ps + 3*W);
    us4.load_a(ps + 4*W);

    // Load Xref
    Xref0.load_a(Xref.data() + 0*W);
    Xref1.load_a(Xref.data() + 1*W);
    Xref2.load_a(Xref.data() + 2*W);
    Xref3.load_a(Xref.data() + 3*W);
    Xref4.load_a(Xref.data() + 4*W);

    // Load H
    H0.load_a(H.data() + 0*W);
    H1.load_a(H.data() + 1*W);
    H2.load_a(H.data() + 2*W);
    H3.load_a(H.data() + 3*W);
    H4.load_a(H.data() + 4*W);

    // load vrefResidual
    vrefResidual.load_a(refResidual.data());

    // load verrorMeas2
    verrorMeas2.load_a(errorMeas2.data());

    res = vrefResidual
      +  H0 * (Xref0 - us0)
      +  H1 * (Xref1 - us1)
      +  H2 * (Xref2 - us2)
      +  H3 * (Xref3 - us3)
      +  H4 * (Xref4 - us4);

    cht0 = uc0 *H0 + uc1 *H1 + uc3 *H2 + uc6 *H3 + uc10*H4;
    cht1 = uc1 *H0 + uc2 *H1 + uc4 *H2 + uc7 *H3 + uc11*H4;
    cht2 = uc3 *H0 + uc4 *H1 + uc5 *H2 + uc8 *H3 + uc12*H4;
    cht3 = uc6 *H0 + uc7 *H1 + uc8 *H2 + uc9 *H3 + uc13*H4;
    cht4 = uc10*H0 + uc11*H1 + uc12*H2 + uc13*H3 + uc14*H4;

    const boolvectype maskedVector = makeBoolHitOnTrack(nodes, std::make_index_sequence<W>());
    const vectype errorResInv2 = select(maskedVector,
      1. / (verrorMeas2
        + H0 * cht0
        + H1 * cht1
        + H2 * cht2
        + H3 * cht3
        + H4 * cht4
    ), 0);

    // update the state vector and cov matrix
    const vectype w = res * errorResInv2;

    us0 += cht0 * w;
    us1 += cht1 * w;
    us2 += cht2 * w;
    us3 += cht3 * w;
    us4 += cht4 * w;

    uc0  -= errorResInv2 * cht0 * cht0;
    uc1  -= errorResInv2 * cht1 * cht0;
    uc2  -= errorResInv2 * cht1 * cht1;
    uc3  -= errorResInv2 * cht2 * cht0;
    uc4  -= errorResInv2 * cht2 * cht1;
    uc5  -= errorResInv2 * cht2 * cht2;
    uc6  -= errorResInv2 * cht3 * cht0;
    uc7  -= errorResInv2 * cht3 * cht1;
    uc8  -= errorResInv2 * cht3 * cht2;
    uc9  -= errorResInv2 * cht3 * cht3;
    uc10 -= errorResInv2 * cht4 * cht0;
    uc11 -= errorResInv2 * cht4 * cht1;
    uc12 -= errorResInv2 * cht4 * cht2;
    uc13 -= errorResInv2 * cht4 * cht3;
    uc14 -= errorResInv2 * cht4 * cht4;

    vchi2 = res * res * errorResInv2;

    // Store uc
    uc0.store_a(uc + 0*W);
    uc1.store_a(uc + 1*W);
    uc2.store_a(uc + 2*W);
    uc3.store_a(uc + 3*W);
    uc4.store_a(uc + 4*W);
    uc5.store_a(uc + 5*W);
    uc6.store_a(uc + 6*W);
    uc7.store_a(uc + 7*W);
    uc8.store_a(uc + 8*W);
    uc9.store_a(uc + 9*W);
    uc10.store_a(uc + 10*W);
    uc11.store_a(uc + 11*W);
    uc12.store_a(uc + 12*W);
    uc13.store_a(uc + 13*W);
    uc14.store_a(uc + 14*W);

    // Store us
    us0.store_a(us + 0*W);
    us1.store_a(us + 1*W);
    us2.store_a(us + 2*W);
    us3.store_a(us + 3*W);
    us4.store_a(us + 4*W);

    // Store chi2
    vchi2.store_a(chi2);
  }

  static uint8_t average (
    double_ptr_64_const X1,
    double_ptr_64_const X2,
    double_ptr_64_const C1,
    double_ptr_64_const C2,
    double_ptr_64 X,
    double_ptr_64 C
  ) {
    using vectype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    vectype K0, K1, K2, K3, K4, K5, K6, K7, K8, K9, K10, K11, K12, K13, K14, K15, K16, K17, K18, K19, K20, K21, K22, K23, K24;
    vectype X1_0, X1_1, X1_2, X1_3, X1_4;
    vectype X2_0, X2_1, X2_2, X2_3, X2_4;
    vectype C1_0, C1_1, C1_2, C1_3, C1_4, C1_5, C1_6, C1_7, C1_8, C1_9, C1_10, C1_11, C1_12, C1_13, C1_14;
    vectype C2_0, C2_1, C2_2, C2_3, C2_4, C2_5, C2_6, C2_7, C2_8, C2_9, C2_10, C2_11, C2_12, C2_13, C2_14;
    vectype Csum0, Csum1, Csum2, Csum3, Csum4, Csum5, Csum6, Csum7, Csum8, Csum9, Csum10, Csum11, Csum12, Csum13, Csum14;
    vectype X_0, X_1, X_2, X_3, X_4;
    vectype C_0, C_1, C_2, C_3, C_4, C_5, C_6, C_7, C_8, C_9, C_10, C_11, C_12, C_13, C_14;
    vectype L0, L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13, L14;
    vectype inv0, inv1, inv2, inv3, inv4, inv5, inv6, inv7, inv8, inv9, inv10, inv11, inv12, inv13, inv14;
    vectype diff0, diff1, diff2, diff3, diff4;

    boolvectype success (true);

    // Load X1
    X1_0.load_a(X1 + 0*W);
    X1_1.load_a(X1 + 1*W);
    X1_2.load_a(X1 + 2*W);
    X1_3.load_a(X1 + 3*W);
    X1_4.load_a(X1 + 4*W);

    // Load X2
    X2_0.load_a(X2 + 0*W);
    X2_1.load_a(X2 + 1*W);
    X2_2.load_a(X2 + 2*W);
    X2_3.load_a(X2 + 3*W);
    X2_4.load_a(X2 + 4*W);

    // Load C1
    C1_0.load_a(C1 + 0*W);
    C1_1.load_a(C1 + 1*W);
    C1_2.load_a(C1 + 2*W);
    C1_3.load_a(C1 + 3*W);
    C1_4.load_a(C1 + 4*W);
    C1_5.load_a(C1 + 5*W);
    C1_6.load_a(C1 + 6*W);
    C1_7.load_a(C1 + 7*W);
    C1_8.load_a(C1 + 8*W);
    C1_9.load_a(C1 + 9*W);
    C1_10.load_a(C1 + 10*W);
    C1_11.load_a(C1 + 11*W);
    C1_12.load_a(C1 + 12*W);
    C1_13.load_a(C1 + 13*W);
    C1_14.load_a(C1 + 14*W);

    // Load C2
    C2_0.load_a(C2 + 0*W);
    C2_1.load_a(C2 + 1*W);
    C2_2.load_a(C2 + 2*W);
    C2_3.load_a(C2 + 3*W);
    C2_4.load_a(C2 + 4*W);
    C2_5.load_a(C2 + 5*W);
    C2_6.load_a(C2 + 6*W);
    C2_7.load_a(C2 + 7*W);
    C2_8.load_a(C2 + 8*W);
    C2_9.load_a(C2 + 9*W);
    C2_10.load_a(C2 + 10*W);
    C2_11.load_a(C2 + 11*W);
    C2_12.load_a(C2 + 12*W);
    C2_13.load_a(C2 + 13*W);
    C2_14.load_a(C2 + 14*W);

    // C sum
    Csum0  = C1_0  + C2_0;
    Csum1  = C1_1  + C2_1;
    Csum2  = C1_2  + C2_2;
    Csum3  = C1_3  + C2_3;
    Csum4  = C1_4  + C2_4;
    Csum5  = C1_5  + C2_5;
    Csum6  = C1_6  + C2_6;
    Csum7  = C1_7  + C2_7;
    Csum8  = C1_8  + C2_8;
    Csum9  = C1_9  + C2_9;
    Csum10 = C1_10 + C2_10;
    Csum11 = C1_11 + C2_11;
    Csum12 = C1_12 + C2_12;
    Csum13 = C1_13 + C2_13;
    Csum14 = C1_14 + C2_14;

    // Factorization
    L0 = Csum0;
    success &= L0 > 0.0;
    L0 = sqrt(1.0 / L0);

    L1  = Csum1  * L0;
    L3  = Csum3  * L0;
    L6  = Csum6  * L0;
    L10 = Csum10 * L0;

    L2 = Csum2 - L1*L1;
    success &= L2 > 0.0;
    L2 = sqrt(1.0 / L2);

    L4  = (Csum4  - L3 *L1) * L2;
    L7  = (Csum7  - L6 *L1) * L2;
    L11 = (Csum11 - L10*L1) * L2;

    L5 = Csum5 - L3*L3 - L4*L4;
    success &= L5 > 0.0;
    L5 = sqrt(1.0 / L5);

    L8  = (Csum8  - L6 *L3 - L7 *L4) * L5;
    L12 = (Csum12 - L10*L3 - L11*L4) * L5;

    L9 = Csum9 - L6*L6 - L7*L7 - L8*L8;
    success &= L9 > 0.0;
    L9 = sqrt(1.0 / L9);

    L13 = (Csum13 - L10*L6 - L11*L7 - L12*L8) * L9;

    L14 = Csum14 - L10*L10 - L11*L11 - L12*L12 - L13*L13;
    success &= L14 > 0.0;
    L14 = sqrt(1.0 / L14);

    // Inversion

    // Forward substitution
    inv0  = L0;
    inv1  = (- L1 *inv0) * L2;
    inv3  = (- L3 *inv0 - L4 *inv1) * L5;
    inv6  = (- L6 *inv0 - L7 *inv1 - L8 *inv3) * L9;
    inv10 = (- L10*inv0 - L11*inv1 - L12*inv3 - L13*inv6) * L14;

    inv2  = L2;
    inv4  = (- L4 *inv2) * L5;
    inv7  = (- L7 *inv2 - L8 *inv4) * L9;
    inv11 = (- L11*inv2 - L12*inv4 - L13*inv7) * L14;

    inv5  = L5;
    inv8  = (- L8 *inv5) * L9;
    inv12 = (- L12*inv5 - L13*inv8) * L14;

    inv9  = L9;
    inv13 = (- L13*inv9) * L14;

    inv14 = L14;

    // Backward substitution
    inv10 = inv10 * L14;
    inv6  = (inv6 - L13*inv10) * L9;
    inv3  = (inv3 - L12*inv10  - L8*inv6) * L5;
    inv1  = (inv1 - L11*inv10  - L7*inv6  - L4*inv3) * L2;
    inv0  = (inv0 - L10*inv10  - L6*inv6  - L3*inv3  - L1*inv1) * L0;

    inv11 = inv11 * L14;
    inv7  = (inv7 - L13*inv11) * L9;
    inv4  = (inv4 - L12*inv11  - L8*inv7) * L5;
    inv2  = (inv2 - L11*inv11  - L7*inv7  - L4*inv4) * L2;

    inv12 = inv12 * L14;
    inv8  = (inv8 - L13*inv12) * L9;
    inv5  = (inv5 - L12*inv12  - L8*inv8) * L5;

    inv13 = inv13 * L14;
    inv9  = (inv9 - L13*inv13) * L9;

    inv14 = inv14 * L14;

    K0  = C1_0 *inv0  + C1_1 *inv1  + C1_3 *inv3  + C1_6 *inv6  + C1_10*inv10;
    K1  = C1_0 *inv1  + C1_1 *inv2  + C1_3 *inv4  + C1_6 *inv7  + C1_10*inv11;
    K2  = C1_0 *inv3  + C1_1 *inv4  + C1_3 *inv5  + C1_6 *inv8  + C1_10*inv12;
    K3  = C1_0 *inv6  + C1_1 *inv7  + C1_3 *inv8  + C1_6 *inv9  + C1_10*inv13;
    K4  = C1_0 *inv10 + C1_1 *inv11 + C1_3 *inv12 + C1_6 *inv13 + C1_10*inv14;
    K5  = C1_1 *inv0  + C1_2 *inv1  + C1_4 *inv3  + C1_7 *inv6  + C1_11*inv10;
    K6  = C1_1 *inv1  + C1_2 *inv2  + C1_4 *inv4  + C1_7 *inv7  + C1_11*inv11;
    K7  = C1_1 *inv3  + C1_2 *inv4  + C1_4 *inv5  + C1_7 *inv8  + C1_11*inv12;
    K8  = C1_1 *inv6  + C1_2 *inv7  + C1_4 *inv8  + C1_7 *inv9  + C1_11*inv13;
    K9  = C1_1 *inv10 + C1_2 *inv11 + C1_4 *inv12 + C1_7 *inv13 + C1_11*inv14;
    K10 = C1_3 *inv0  + C1_4 *inv1  + C1_5 *inv3  + C1_8 *inv6  + C1_12*inv10;
    K11 = C1_3 *inv1  + C1_4 *inv2  + C1_5 *inv4  + C1_8 *inv7  + C1_12*inv11;
    K12 = C1_3 *inv3  + C1_4 *inv4  + C1_5 *inv5  + C1_8 *inv8  + C1_12*inv12;
    K13 = C1_3 *inv6  + C1_4 *inv7  + C1_5 *inv8  + C1_8 *inv9  + C1_12*inv13;
    K14 = C1_3 *inv10 + C1_4 *inv11 + C1_5 *inv12 + C1_8 *inv13 + C1_12*inv14;
    K15 = C1_6 *inv0  + C1_7 *inv1  + C1_8 *inv3  + C1_9 *inv6  + C1_13*inv10;
    K16 = C1_6 *inv1  + C1_7 *inv2  + C1_8 *inv4  + C1_9 *inv7  + C1_13*inv11;
    K17 = C1_6 *inv3  + C1_7 *inv4  + C1_8 *inv5  + C1_9 *inv8  + C1_13*inv12;
    K18 = C1_6 *inv6  + C1_7 *inv7  + C1_8 *inv8  + C1_9 *inv9  + C1_13*inv13;
    K19 = C1_6 *inv10 + C1_7 *inv11 + C1_8 *inv12 + C1_9 *inv13 + C1_13*inv14;
    K20 = C1_10*inv0  + C1_11*inv1  + C1_12*inv3  + C1_13*inv6  + C1_14*inv10;
    K21 = C1_10*inv1  + C1_11*inv2  + C1_12*inv4  + C1_13*inv7  + C1_14*inv11;
    K22 = C1_10*inv3  + C1_11*inv4  + C1_12*inv5  + C1_13*inv8  + C1_14*inv12;
    K23 = C1_10*inv6  + C1_11*inv7  + C1_12*inv8  + C1_13*inv9  + C1_14*inv13;
    K24 = C1_10*inv10 + C1_11*inv11 + C1_12*inv12 + C1_13*inv13 + C1_14*inv14;

    // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
    diff0 = X2_0 - X1_0;
    diff1 = X2_1 - X1_1;
    diff2 = X2_2 - X1_2;
    diff3 = X2_3 - X1_3;
    diff4 = X2_4 - X1_4;

    X_0 = X1_0 + K0 *diff0 + K1 *diff1 + K2 *diff2 + K3 *diff3 + K4 *diff4;
    X_1 = X1_1 + K5 *diff0 + K6 *diff1 + K7 *diff2 + K8 *diff3 + K9 *diff4;
    X_2 = X1_2 + K10*diff0 + K11*diff1 + K12*diff2 + K13*diff3 + K14*diff4;
    X_3 = X1_3 + K15*diff0 + K16*diff1 + K17*diff2 + K18*diff3 + K19*diff4;
    X_4 = X1_4 + K20*diff0 + K21*diff1 + K22*diff2 + K23*diff3 + K24*diff4;

    // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
    C_0  = K0 *C2_0  + K1 *C2_1  + K2 *C2_3  + K3 *C2_6  + K4 *C2_10;
    C_1  = K5 *C2_0  + K6 *C2_1  + K7 *C2_3  + K8 *C2_6  + K9 *C2_10;
    C_2  = K5 *C2_1  + K6 *C2_2  + K7 *C2_4  + K8 *C2_7  + K9 *C2_11;
    C_3  = K10*C2_0  + K11*C2_1  + K12*C2_3  + K13*C2_6  + K14*C2_10;
    C_4  = K10*C2_1  + K11*C2_2  + K12*C2_4  + K13*C2_7  + K14*C2_11;
    C_5  = K10*C2_3  + K11*C2_4  + K12*C2_5  + K13*C2_8  + K14*C2_12;
    C_6  = K15*C2_0  + K16*C2_1  + K17*C2_3  + K18*C2_6  + K19*C2_10;
    C_7  = K15*C2_1  + K16*C2_2  + K17*C2_4  + K18*C2_7  + K19*C2_11;
    C_8  = K15*C2_3  + K16*C2_4  + K17*C2_5  + K18*C2_8  + K19*C2_12;
    C_9  = K15*C2_6  + K16*C2_7  + K17*C2_8  + K18*C2_9  + K19*C2_13;
    C_10 = K20*C2_0  + K21*C2_1  + K22*C2_3  + K23*C2_6  + K24*C2_10; 
    C_11 = K20*C2_1  + K21*C2_2  + K22*C2_4  + K23*C2_7  + K24*C2_11; 
    C_12 = K20*C2_3  + K21*C2_4  + K22*C2_5  + K23*C2_8  + K24*C2_12; 
    C_13 = K20*C2_6  + K21*C2_7  + K22*C2_8  + K23*C2_9  + K24*C2_13; 
    C_14 = K20*C2_10 + K21*C2_11 + K22*C2_12 + K23*C2_13 + K24*C2_14;

    // Store X
    X_0.store_a(X + 0*W);
    X_1.store_a(X + 1*W);
    X_2.store_a(X + 2*W);
    X_3.store_a(X + 3*W);
    X_4.store_a(X + 4*W);

    // Store C
    C_0.store_a(C + 0*W);
    C_1.store_a(C + 1*W);
    C_2.store_a(C + 2*W);
    C_3.store_a(C + 3*W);
    C_4.store_a(C + 4*W);
    C_5.store_a(C + 5*W);
    C_6.store_a(C + 6*W);
    C_7.store_a(C + 7*W);
    C_8.store_a(C + 8*W);
    C_9.store_a(C + 9*W);
    C_10.store_a(C + 10*W);
    C_11.store_a(C + 11*W);
    C_12.store_a(C + 12*W);
    C_13.store_a(C + 13*W);
    C_14.store_a(C + 14*W);

    return to_bits(success);
  }

  static void updateResiduals (
    const std::array<SchItem, W>& _aligned nodes,
    const std::array<double, 5*W>& _aligned pm,
    const std::array<double, 5*W>& _aligned pa,
    const std::array<double, W>& _aligned em,
    const std::array<double, W>& _aligned rr,
    double_ptr_64_const ss,
    double_ptr_64_const sc,
    double_ptr_64 res,
    double_ptr_64 errRes
  ) {
    using vectype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    vectype pm0, pm1, pm2, pm3, pm4;
    vectype pa0, pa1, pa2, pa3, pa4;
    vectype ss0, ss1, ss2, ss3, ss4;
    vectype sc0, sc1, sc2, sc3, sc4, sc5, sc6, sc7, sc8, sc9, sc10, sc11, sc12, sc13, sc14;
    vectype v0, v1, v2, v3, v4;
    vectype vres, verrRes, vem, vrr, vHCH, vsign, vvalue, verror;


    // Load pm
    pm0.load_a(pm.data() + 0*W);
    pm1.load_a(pm.data() + 1*W);
    pm2.load_a(pm.data() + 2*W);
    pm3.load_a(pm.data() + 3*W);
    pm4.load_a(pm.data() + 4*W);

    // Load pa
    pa0.load_a(pa.data() + 0*W);
    pa1.load_a(pa.data() + 1*W);
    pa2.load_a(pa.data() + 2*W);
    pa3.load_a(pa.data() + 3*W);
    pa4.load_a(pa.data() + 4*W);

    // Load ss
    ss0.load_a(ss + 0*W);
    ss1.load_a(ss + 1*W);
    ss2.load_a(ss + 2*W);
    ss3.load_a(ss + 3*W);
    ss4.load_a(ss + 4*W);

    // Load sc
    sc0.load_a(sc + 0*W);
    sc1.load_a(sc + 1*W);
    sc2.load_a(sc + 2*W);
    sc3.load_a(sc + 3*W);
    sc4.load_a(sc + 4*W);
    sc5.load_a(sc + 5*W);
    sc6.load_a(sc + 6*W);
    sc7.load_a(sc + 7*W);
    sc8.load_a(sc + 8*W);
    sc9.load_a(sc + 9*W);
    sc10.load_a(sc + 10*W);
    sc11.load_a(sc + 11*W);
    sc12.load_a(sc + 12*W);
    sc13.load_a(sc + 13*W);
    sc14.load_a(sc + 14*W);

    // Load em
    vem.load_a(em.data());

    // Load rr
    vrr.load_a(rr.data());

    // HCH = pm[ 0]*_0 + pm[ 1]*_1 + pm[ 2]*_2 + pm[ 3]*_3 + pm[ 4]*_4;
    v0 = sc0 *pm0+sc1 *pm1+sc3 *pm2+sc6 *pm3+sc10*pm4;
    v1 = sc1 *pm0+sc2 *pm1+sc4 *pm2+sc7 *pm3+sc11*pm4;
    v2 = sc3 *pm0+sc4 *pm1+sc5 *pm2+sc8 *pm3+sc12*pm4;
    v3 = sc6 *pm0+sc7 *pm1+sc8 *pm2+sc9 *pm3+sc13*pm4;
    v4 = sc10*pm0+sc11*pm1+sc12*pm2+sc13*pm3+sc14*pm4;
    vHCH = pm0*v0 + pm1*v1 + pm2*v2 + pm3*v3 + pm4*v4;

    // const double V = node.m_errMeasure * node.m_errMeasure;
    // const double sign = node.m_type == HitOnTrack ? -1 : 1;
    const boolvectype maskedHitOnTrack = makeBoolHitOnTrack(nodes, std::make_index_sequence<W>());
    vsign = select(maskedHitOnTrack, -1.0, 1.0);

    // const TrackVector& refX = node.m_refVector.m_parameters;
    // value = node.m_refResidual + (pm * (pa - node.getSmooth<Fit::StateVector>()));
    vvalue = vrr + (
        pm0 * (pa0 - ss0) +
        pm1 * (pa1 - ss1) +
        pm2 * (pa2 - ss2) +
        pm3 * (pa3 - ss3) +
        pm4 * (pa4 - ss4)
    );

    // error = V + sign * HCH;
    verror = vem + vsign * vHCH;

    // Bring back the changes
    const boolvectype maskedMeasurement = makeBoolMeasurement(nodes, std::make_index_sequence<W>());
    vres    = select(maskedMeasurement, vvalue, 0.0);
    verrRes = select(maskedMeasurement, verror, 0.0);

    vres.store_a(res);
    verrRes.store_a(errRes);
  }
};

template<class T>
struct FitMath {
  template<unsigned W>
  static void predictState (
    const std::array<double, 25*W>& _aligned tm,
    const std::array<double, 5*W>& _aligned tv,
    double_ptr_64_const us,
    double_ptr_64 ps
  );

  template<unsigned W>
  static void predictCovariance (
    const std::array<double, 25*W>& tm,
    const std::array<double, 15*W>& nm,
    double_ptr_64_const uc,
    double_ptr_64 pc
  );
};

template<>
struct FitMath<Fit::Forward> {
  template<unsigned W>
  static void predictState (
    const std::array<double, 25*W>& _aligned tm,
    const std::array<double, 5*W>& _aligned tv,
    double_ptr_64_const us,
    double_ptr_64 ps
  ) {
    using vectype = typename Vectype<W>::type;

    vectype tm0, tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13, tm14, tm15, tm16, tm17, tm18, tm19, tm20, tm21, tm22, tm23, tm24;
    vectype tv0, tv1, tv2, tv3, tv4;
    vectype us0, us1, us2, us3, us4;
    vectype ps0, ps1, ps2, ps3, ps4;

    // Load tm
    tm0.load_a(tm.data() + 0*W);
    tm1.load_a(tm.data() + 1*W);
    tm2.load_a(tm.data() + 2*W);
    tm3.load_a(tm.data() + 3*W);
    tm4.load_a(tm.data() + 4*W);
    tm5.load_a(tm.data() + 5*W);
    tm6.load_a(tm.data() + 6*W);
    tm7.load_a(tm.data() + 7*W);
    tm8.load_a(tm.data() + 8*W);
    tm9.load_a(tm.data() + 9*W);
    tm10.load_a(tm.data() + 10*W);
    tm11.load_a(tm.data() + 11*W);
    tm12.load_a(tm.data() + 12*W);
    tm13.load_a(tm.data() + 13*W);
    tm14.load_a(tm.data() + 14*W);
    tm15.load_a(tm.data() + 15*W);
    tm16.load_a(tm.data() + 16*W);
    tm17.load_a(tm.data() + 17*W);
    tm18.load_a(tm.data() + 18*W);
    tm19.load_a(tm.data() + 19*W);
    tm20.load_a(tm.data() + 20*W);
    tm21.load_a(tm.data() + 21*W);
    tm22.load_a(tm.data() + 22*W);
    tm23.load_a(tm.data() + 23*W);
    tm24.load_a(tm.data() + 24*W);

    // Load tv
    tv0.load_a(tv.data() + 0*W);
    tv1.load_a(tv.data() + 1*W);
    tv2.load_a(tv.data() + 2*W);
    tv3.load_a(tv.data() + 3*W);
    tv4.load_a(tv.data() + 4*W);

    // Load us
    us0.load_a(us + 0*W);
    us1.load_a(us + 1*W);
    us2.load_a(us + 2*W);
    us3.load_a(us + 3*W);
    us4.load_a(us + 4*W);

    ps0 = tv0 + tm0  * us0 + tm1  * us1 + tm2  * us2 + tm3  * us3 + tm4  * us4;
    ps1 = tv1 + tm5  * us0 + tm6  * us1 + tm7  * us2 + tm8  * us3 + tm9  * us4;
    ps2 = tv2 + tm10 * us0 + tm11 * us1 + tm12 * us2 + tm13 * us3 + tm14 * us4;
    ps3 = tv3 + tm15 * us0 + tm16 * us1 + tm17 * us2 + tm18 * us3 + tm19 * us4;
    ps4 = tv4 + tm20 * us0 + tm21 * us1 + tm22 * us2 + tm23 * us3 + tm24 * us4;

    // Store ps
    ps0.store_a(ps + 0*W);
    ps1.store_a(ps + 1*W);
    ps2.store_a(ps + 2*W);
    ps3.store_a(ps + 3*W);
    ps4.store_a(ps + 4*W);
  }

  template<unsigned W>
  static void predictCovariance (
    const std::array<double, 25*W>& tm,
    const std::array<double, 15*W>& nm,
    double_ptr_64_const uc,
    double_ptr_64 pc
  ) {
    // Delegate to similarity_5_5 implementation
    FitMathCommon<W>::similarity_5_5(tm, uc, pc);

    using vectype = typename Vectype<W>::type;
    (vectype().load_a(pc +  0*W) + vectype().load_a(nm.data() +  0*W)).store_a(pc +  0*W);
    (vectype().load_a(pc +  1*W) + vectype().load_a(nm.data() +  1*W)).store_a(pc +  1*W);
    (vectype().load_a(pc +  2*W) + vectype().load_a(nm.data() +  2*W)).store_a(pc +  2*W);
    (vectype().load_a(pc +  3*W) + vectype().load_a(nm.data() +  3*W)).store_a(pc +  3*W);
    (vectype().load_a(pc +  4*W) + vectype().load_a(nm.data() +  4*W)).store_a(pc +  4*W);
    (vectype().load_a(pc +  5*W) + vectype().load_a(nm.data() +  5*W)).store_a(pc +  5*W);
    (vectype().load_a(pc +  6*W) + vectype().load_a(nm.data() +  6*W)).store_a(pc +  6*W);
    (vectype().load_a(pc +  7*W) + vectype().load_a(nm.data() +  7*W)).store_a(pc +  7*W);
    (vectype().load_a(pc +  8*W) + vectype().load_a(nm.data() +  8*W)).store_a(pc +  8*W);
    (vectype().load_a(pc +  9*W) + vectype().load_a(nm.data() +  9*W)).store_a(pc +  9*W);
    (vectype().load_a(pc + 10*W) + vectype().load_a(nm.data() + 10*W)).store_a(pc + 10*W);
    (vectype().load_a(pc + 11*W) + vectype().load_a(nm.data() + 11*W)).store_a(pc + 11*W);
    (vectype().load_a(pc + 12*W) + vectype().load_a(nm.data() + 12*W)).store_a(pc + 12*W);
    (vectype().load_a(pc + 13*W) + vectype().load_a(nm.data() + 13*W)).store_a(pc + 13*W);
    (vectype().load_a(pc + 14*W) + vectype().load_a(nm.data() + 14*W)).store_a(pc + 14*W);
  }
};

template<>
struct FitMath<Fit::Backward> {
  template<unsigned W>
  static void predictState (
    const std::array<double, 25*W>& _aligned tm,
    const std::array<double, 5*W>& _aligned tv,
    double_ptr_64_const us,
    double_ptr_64 ps
  ) {
    using vectype = typename Vectype<W>::type;

    vectype tm0, tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13, tm14, tm15, tm16, tm17, tm18, tm19, tm20, tm21, tm22, tm23, tm24;
    vectype tv0, tv1, tv2, tv3, tv4;
    vectype us0, us1, us2, us3, us4;
    vectype ps0, ps1, ps2, ps3, ps4;
    vectype s0, s1, s2, s3, s4;

    // Load tm
    tm0.load_a(tm.data() + 0*W);
    tm1.load_a(tm.data() + 1*W);
    tm2.load_a(tm.data() + 2*W);
    tm3.load_a(tm.data() + 3*W);
    tm4.load_a(tm.data() + 4*W);
    tm5.load_a(tm.data() + 5*W);
    tm6.load_a(tm.data() + 6*W);
    tm7.load_a(tm.data() + 7*W);
    tm8.load_a(tm.data() + 8*W);
    tm9.load_a(tm.data() + 9*W);
    tm10.load_a(tm.data() + 10*W);
    tm11.load_a(tm.data() + 11*W);
    tm12.load_a(tm.data() + 12*W);
    tm13.load_a(tm.data() + 13*W);
    tm14.load_a(tm.data() + 14*W);
    tm15.load_a(tm.data() + 15*W);
    tm16.load_a(tm.data() + 16*W);
    tm17.load_a(tm.data() + 17*W);
    tm18.load_a(tm.data() + 18*W);
    tm19.load_a(tm.data() + 19*W);
    tm20.load_a(tm.data() + 20*W);
    tm21.load_a(tm.data() + 21*W);
    tm22.load_a(tm.data() + 22*W);
    tm23.load_a(tm.data() + 23*W);
    tm24.load_a(tm.data() + 24*W);

    // Load tv
    tv0.load_a(tv.data() + 0*W);
    tv1.load_a(tv.data() + 1*W);
    tv2.load_a(tv.data() + 2*W);
    tv3.load_a(tv.data() + 3*W);
    tv4.load_a(tv.data() + 4*W);

    // Load us
    us0.load_a(us + 0*W);
    us1.load_a(us + 1*W);
    us2.load_a(us + 2*W);
    us3.load_a(us + 3*W);
    us4.load_a(us + 4*W);

    s0 = us0 - tv0;
    s1 = us1 - tv1;
    s2 = us2 - tv2;
    s3 = us3 - tv3;
    s4 = us4 - tv4;

    ps0 = tm0  * s0 + tm1  * s1 + tm2  * s2 + tm3  * s3 + tm4  * s4;
    ps1 = tm5  * s0 + tm6  * s1 + tm7  * s2 + tm8  * s3 + tm9  * s4;
    ps2 = tm10 * s0 + tm11 * s1 + tm12 * s2 + tm13 * s3 + tm14 * s4;
    ps3 = tm15 * s0 + tm16 * s1 + tm17 * s2 + tm18 * s3 + tm19 * s4;
    ps4 = tm20 * s0 + tm21 * s1 + tm22 * s2 + tm23 * s3 + tm24 * s4;

    // Store ps
    ps0.store_a(ps + 0*W);
    ps1.store_a(ps + 1*W);
    ps2.store_a(ps + 2*W);
    ps3.store_a(ps + 3*W);
    ps4.store_a(ps + 4*W);
  }

  template<unsigned W>
  static void predictCovariance (
    const std::array<double, 25*W>& tm,
    std::array<double, 15*W>& nm,
    double_ptr_64_const uc,
    double_ptr_64 pc
  ) {
    using vectype = typename Vectype<W>::type;
    (vectype().load_a(nm.data() +  0*W) + vectype().load_a(uc +  0*W)).store_a(nm.data() +  0*W);
    (vectype().load_a(nm.data() +  1*W) + vectype().load_a(uc +  1*W)).store_a(nm.data() +  1*W);
    (vectype().load_a(nm.data() +  2*W) + vectype().load_a(uc +  2*W)).store_a(nm.data() +  2*W);
    (vectype().load_a(nm.data() +  3*W) + vectype().load_a(uc +  3*W)).store_a(nm.data() +  3*W);
    (vectype().load_a(nm.data() +  4*W) + vectype().load_a(uc +  4*W)).store_a(nm.data() +  4*W);
    (vectype().load_a(nm.data() +  5*W) + vectype().load_a(uc +  5*W)).store_a(nm.data() +  5*W);
    (vectype().load_a(nm.data() +  6*W) + vectype().load_a(uc +  6*W)).store_a(nm.data() +  6*W);
    (vectype().load_a(nm.data() +  7*W) + vectype().load_a(uc +  7*W)).store_a(nm.data() +  7*W);
    (vectype().load_a(nm.data() +  8*W) + vectype().load_a(uc +  8*W)).store_a(nm.data() +  8*W);
    (vectype().load_a(nm.data() +  9*W) + vectype().load_a(uc +  9*W)).store_a(nm.data() +  9*W);
    (vectype().load_a(nm.data() + 10*W) + vectype().load_a(uc + 10*W)).store_a(nm.data() + 10*W);
    (vectype().load_a(nm.data() + 11*W) + vectype().load_a(uc + 11*W)).store_a(nm.data() + 11*W);
    (vectype().load_a(nm.data() + 12*W) + vectype().load_a(uc + 12*W)).store_a(nm.data() + 12*W);
    (vectype().load_a(nm.data() + 13*W) + vectype().load_a(uc + 13*W)).store_a(nm.data() + 13*W);
    (vectype().load_a(nm.data() + 14*W) + vectype().load_a(uc + 14*W)).store_a(nm.data() + 14*W);

    // Delegate to similarity_5_5 implementation
    FitMathCommon<W>::similarity_5_5(tm, nm.data(), pc);
  }
};

template<class T>
void math_similarity_5_5_nonvec (
  const TrackMatrix& tm,
  const T& uc,
  nTrackSymMatrix& pc
) {
  auto _0 = uc[ 0]*tm[0]+uc[ 1]*tm[1]+uc[ 3]*tm[2]+uc[ 6]*tm[3]+uc[10]*tm[4];
  auto _1 = uc[ 1]*tm[0]+uc[ 2]*tm[1]+uc[ 4]*tm[2]+uc[ 7]*tm[3]+uc[11]*tm[4];
  auto _2 = uc[ 3]*tm[0]+uc[ 4]*tm[1]+uc[ 5]*tm[2]+uc[ 8]*tm[3]+uc[12]*tm[4];
  auto _3 = uc[ 6]*tm[0]+uc[ 7]*tm[1]+uc[ 8]*tm[2]+uc[ 9]*tm[3]+uc[13]*tm[4];
  auto _4 = uc[10]*tm[0]+uc[11]*tm[1]+uc[12]*tm[2]+uc[13]*tm[3]+uc[14]*tm[4];
  pc[ 0] = tm[ 0]*_0 + tm[ 1]*_1 + tm[ 2]*_2 + tm[ 3]*_3 + tm[ 4]*_4;
  pc[ 1] = tm[ 5]*_0 + tm[ 6]*_1 + tm[ 7]*_2 + tm[ 8]*_3 + tm[ 9]*_4;
  pc[ 3] = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
  pc[ 6] = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[10] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4; 
  _0 =  uc[ 0]*tm[5]+uc[ 1]*tm[6]+uc[ 3]*tm[7]+uc[ 6]*tm[8]+uc[10]*tm[9];
  _1 =  uc[ 1]*tm[5]+uc[ 2]*tm[6]+uc[ 4]*tm[7]+uc[ 7]*tm[8]+uc[11]*tm[9];
  _2 =  uc[ 3]*tm[5]+uc[ 4]*tm[6]+uc[ 5]*tm[7]+uc[ 8]*tm[8]+uc[12]*tm[9];
  _3 =  uc[ 6]*tm[5]+uc[ 7]*tm[6]+uc[ 8]*tm[7]+uc[ 9]*tm[8]+uc[13]*tm[9];
  _4 =  uc[10]*tm[5]+uc[11]*tm[6]+uc[12]*tm[7]+uc[13]*tm[8]+uc[14]*tm[9];
  pc[2]  = tm[ 5]*_0 + tm[ 6]*_1 + tm[ 7]*_2 + tm[ 8]*_3 + tm[ 9]*_4;
  pc[4]  = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
  pc[7]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[11] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
  _0 = uc[ 0]*tm[10]+uc[ 1]*tm[11]+uc[ 3]*tm[12]+uc[ 6]*tm[13]+uc[10]*tm[14];
  _1 = uc[ 1]*tm[10]+uc[ 2]*tm[11]+uc[ 4]*tm[12]+uc[ 7]*tm[13]+uc[11]*tm[14];
  _2 = uc[ 3]*tm[10]+uc[ 4]*tm[11]+uc[ 5]*tm[12]+uc[ 8]*tm[13]+uc[12]*tm[14];
  _3 = uc[ 6]*tm[10]+uc[ 7]*tm[11]+uc[ 8]*tm[12]+uc[ 9]*tm[13]+uc[13]*tm[14];
  _4 = uc[10]*tm[10]+uc[11]*tm[11]+uc[12]*tm[12]+uc[13]*tm[13]+uc[14]*tm[14];
  pc[5]  = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
  pc[8]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[12] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
  _0 = uc[ 0]*tm[15]+uc[ 1]*tm[16]+uc[ 3]*tm[17]+uc[ 6]*tm[18]+uc[10]*tm[19];
  _1 = uc[ 1]*tm[15]+uc[ 2]*tm[16]+uc[ 4]*tm[17]+uc[ 7]*tm[18]+uc[11]*tm[19];
  _2 = uc[ 3]*tm[15]+uc[ 4]*tm[16]+uc[ 5]*tm[17]+uc[ 8]*tm[18]+uc[12]*tm[19];
  _3 = uc[ 6]*tm[15]+uc[ 7]*tm[16]+uc[ 8]*tm[17]+uc[ 9]*tm[18]+uc[13]*tm[19];
  _4 = uc[10]*tm[15]+uc[11]*tm[16]+uc[12]*tm[17]+uc[13]*tm[18]+uc[14]*tm[19];
  pc[9]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[13] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
  _0 = uc[ 0]*tm[20]+uc[1]*tm[21]+uc[3]*tm[22]+uc[6]*tm[23]+uc[10]*tm[24];
  _1 = uc[ 1]*tm[20]+uc[2]*tm[21]+uc[4]*tm[22]+uc[7]*tm[23]+uc[11]*tm[24];
  _2 = uc[ 3]*tm[20]+uc[4]*tm[21]+uc[5]*tm[22]+uc[8]*tm[23]+uc[12]*tm[24];
  _3 = uc[ 6]*tm[20]+uc[7]*tm[21]+uc[8]*tm[22]+uc[9]*tm[23]+uc[13]*tm[24];
  _4 = uc[10]*tm[20]+uc[11]*tm[21]+uc[12]*tm[22]+uc[13]*tm[23]+uc[14]*tm[24];
  pc[14]= tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
}

void math_update_nonvec (
  nTrackVector& X,
  nTrackSymMatrix& C,
  double& chi2,
  const double* Xref,
  const double* H,
  double refResidual,
  double errorMeas2
);

template<class S, class T>
void math_average_nonvec (
  const S& X1,
  const T& C1,
  const S& X2,
  const T& C2,
  S& X,
  T& C
) {
  // compute the inverse of the covariance (i.e. weight) of the difference: R=(C1+C2)
  SymMatrix5x5 invRM;
  auto& invR = invRM.fArray;
  for (int i=0; i<15; ++i) {
    invR[i] = C1[i] + C2[i];
  }

  bool success = invRM.InvertChol();

  // compute the gain matrix

  // K <- C1*inverse(C1+C2) = C1*invR
  double K[25];
  K[ 0] = C1[ 0]*invR[ 0] + C1[ 1]*invR[ 1] + C1[ 3]*invR[ 3] + C1[ 6]*invR[ 6] + C1[10]*invR[10];
  K[ 1] = C1[ 0]*invR[ 1] + C1[ 1]*invR[ 2] + C1[ 3]*invR[ 4] + C1[ 6]*invR[ 7] + C1[10]*invR[11];
  K[ 2] = C1[ 0]*invR[ 3] + C1[ 1]*invR[ 4] + C1[ 3]*invR[ 5] + C1[ 6]*invR[ 8] + C1[10]*invR[12];
  K[ 3] = C1[ 0]*invR[ 6] + C1[ 1]*invR[ 7] + C1[ 3]*invR[ 8] + C1[ 6]*invR[ 9] + C1[10]*invR[13];
  K[ 4] = C1[ 0]*invR[10] + C1[ 1]*invR[11] + C1[ 3]*invR[12] + C1[ 6]*invR[13] + C1[10]*invR[14];

  K[ 5] = C1[ 1]*invR[ 0] + C1[ 2]*invR[ 1] + C1[ 4]*invR[ 3] + C1[ 7]*invR[ 6] + C1[11]*invR[10];
  K[ 6] = C1[ 1]*invR[ 1] + C1[ 2]*invR[ 2] + C1[ 4]*invR[ 4] + C1[ 7]*invR[ 7] + C1[11]*invR[11];
  K[ 7] = C1[ 1]*invR[ 3] + C1[ 2]*invR[ 4] + C1[ 4]*invR[ 5] + C1[ 7]*invR[ 8] + C1[11]*invR[12];
  K[ 8] = C1[ 1]*invR[ 6] + C1[ 2]*invR[ 7] + C1[ 4]*invR[ 8] + C1[ 7]*invR[ 9] + C1[11]*invR[13];
  K[ 9] = C1[ 1]*invR[10] + C1[ 2]*invR[11] + C1[ 4]*invR[12] + C1[ 7]*invR[13] + C1[11]*invR[14];

  K[10] = C1[ 3]*invR[ 0] + C1[ 4]*invR[ 1] + C1[ 5]*invR[ 3] + C1[ 8]*invR[ 6] + C1[12]*invR[10];
  K[11] = C1[ 3]*invR[ 1] + C1[ 4]*invR[ 2] + C1[ 5]*invR[ 4] + C1[ 8]*invR[ 7] + C1[12]*invR[11];
  K[12] = C1[ 3]*invR[ 3] + C1[ 4]*invR[ 4] + C1[ 5]*invR[ 5] + C1[ 8]*invR[ 8] + C1[12]*invR[12];
  K[13] = C1[ 3]*invR[ 6] + C1[ 4]*invR[ 7] + C1[ 5]*invR[ 8] + C1[ 8]*invR[ 9] + C1[12]*invR[13];
  K[14] = C1[ 3]*invR[10] + C1[ 4]*invR[11] + C1[ 5]*invR[12] + C1[ 8]*invR[13] + C1[12]*invR[14];

  K[15] = C1[ 6]*invR[ 0] + C1[ 7]*invR[ 1] + C1[ 8]*invR[ 3] + C1[ 9]*invR[ 6] + C1[13]*invR[10];
  K[16] = C1[ 6]*invR[ 1] + C1[ 7]*invR[ 2] + C1[ 8]*invR[ 4] + C1[ 9]*invR[ 7] + C1[13]*invR[11];
  K[17] = C1[ 6]*invR[ 3] + C1[ 7]*invR[ 4] + C1[ 8]*invR[ 5] + C1[ 9]*invR[ 8] + C1[13]*invR[12];
  K[18] = C1[ 6]*invR[ 6] + C1[ 7]*invR[ 7] + C1[ 8]*invR[ 8] + C1[ 9]*invR[ 9] + C1[13]*invR[13];
  K[19] = C1[ 6]*invR[10] + C1[ 7]*invR[11] + C1[ 8]*invR[12] + C1[ 9]*invR[13] + C1[13]*invR[14];

  K[20] = C1[10]*invR[ 0] + C1[11]*invR[ 1] + C1[12]*invR[ 3] + C1[13]*invR[ 6] + C1[14]*invR[10];
  K[21] = C1[10]*invR[ 1] + C1[11]*invR[ 2] + C1[12]*invR[ 4] + C1[13]*invR[ 7] + C1[14]*invR[11];
  K[22] = C1[10]*invR[ 3] + C1[11]*invR[ 4] + C1[12]*invR[ 5] + C1[13]*invR[ 8] + C1[14]*invR[12];
  K[23] = C1[10]*invR[ 6] + C1[11]*invR[ 7] + C1[12]*invR[ 8] + C1[13]*invR[ 9] + C1[14]*invR[13];
  K[24] = C1[10]*invR[10] + C1[11]*invR[11] + C1[12]*invR[12] + C1[13]*invR[13] + C1[14]*invR[14];

  // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
  double d[5] { X2[0]-X1[0], X2[1]-X1[1], X2[2]-X1[2], X2[3]-X1[3], X2[4]-X1[4] };
  X[0] = X1[0] + K[ 0]*d[0] + K[ 1]*d[1] + K[ 2]*d[2] + K[ 3]*d[3] + K[ 4]*d[4];
  X[1] = X1[1] + K[ 5]*d[0] + K[ 6]*d[1] + K[ 7]*d[2] + K[ 8]*d[3] + K[ 9]*d[4];
  X[2] = X1[2] + K[10]*d[0] + K[11]*d[1] + K[12]*d[2] + K[13]*d[3] + K[14]*d[4];
  X[3] = X1[3] + K[15]*d[0] + K[16]*d[1] + K[17]*d[2] + K[18]*d[3] + K[19]*d[4];
  X[4] = X1[4] + K[20]*d[0] + K[21]*d[1] + K[22]*d[2] + K[23]*d[3] + K[24]*d[4];

  // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
  C[ 0] = K[ 0]*C2[ 0] + K[ 1]*C2[ 1] + K[ 2]*C2[ 3] + K[ 3]*C2[ 6] + K[ 4]*C2[10];
  C[ 1] = K[ 5]*C2[ 0] + K[ 6]*C2[ 1] + K[ 7]*C2[ 3] + K[ 8]*C2[ 6] + K[ 9]*C2[10];
  C[ 3] = K[10]*C2[ 0] + K[11]*C2[ 1] + K[12]*C2[ 3] + K[13]*C2[ 6] + K[14]*C2[10];
  C[ 6] = K[15]*C2[ 0] + K[16]*C2[ 1] + K[17]*C2[ 3] + K[18]*C2[ 6] + K[19]*C2[10];
  C[10] = K[20]*C2[ 0] + K[21]*C2[ 1] + K[22]*C2[ 3] + K[23]*C2[ 6] + K[24]*C2[10]; 

  C[ 2] = K[ 5]*C2[ 1] + K[ 6]*C2[ 2] + K[ 7]*C2[ 4] + K[ 8]*C2[ 7] + K[ 9]*C2[11];
  C[ 4] = K[10]*C2[ 1] + K[11]*C2[ 2] + K[12]*C2[ 4] + K[13]*C2[ 7] + K[14]*C2[11];
  C[ 7] = K[15]*C2[ 1] + K[16]*C2[ 2] + K[17]*C2[ 4] + K[18]*C2[ 7] + K[19]*C2[11];
  C[11] = K[20]*C2[ 1] + K[21]*C2[ 2] + K[22]*C2[ 4] + K[23]*C2[ 7] + K[24]*C2[11]; 

  C[ 5] = K[10]*C2[ 3] + K[11]*C2[ 4] + K[12]*C2[ 5] + K[13]*C2[ 8] + K[14]*C2[12];
  C[ 8] = K[15]*C2[ 3] + K[16]*C2[ 4] + K[17]*C2[ 5] + K[18]*C2[ 8] + K[19]*C2[12];
  C[12] = K[20]*C2[ 3] + K[21]*C2[ 4] + K[22]*C2[ 5] + K[23]*C2[ 8] + K[24]*C2[12]; 

  C[ 9] = K[15]*C2[ 6] + K[16]*C2[ 7] + K[17]*C2[ 8] + K[18]*C2[ 9] + K[19]*C2[13];
  C[13] = K[20]*C2[ 6] + K[21]*C2[ 7] + K[22]*C2[ 8] + K[23]*C2[ 9] + K[24]*C2[13]; 

  C[14] = K[20]*C2[10] + K[21]*C2[11] + K[22]*C2[12] + K[23]*C2[13] + K[24]*C2[14]; 
  // the following used to be more stable, but isn't any longer, it seems:
  //ROOT::Math::AssignSym::Evaluate(C, -2 * K * C1) ;
  //C += C1 + ROOT::Math::Similarity(K,R) ;
}

template<class T>
void math_similarity_5_1_nonvec (
  const T& Ci,
  const double* Fi,
  double* ti
) {
  auto _0 = Ci[ 0]*Fi[0]+Ci[ 1]*Fi[1]+Ci[ 3]*Fi[2]+Ci[ 6]*Fi[3]+Ci[10]*Fi[4];
  auto _1 = Ci[ 1]*Fi[0]+Ci[ 2]*Fi[1]+Ci[ 4]*Fi[2]+Ci[ 7]*Fi[3]+Ci[11]*Fi[4];
  auto _2 = Ci[ 3]*Fi[0]+Ci[ 4]*Fi[1]+Ci[ 5]*Fi[2]+Ci[ 8]*Fi[3]+Ci[12]*Fi[4];
  auto _3 = Ci[ 6]*Fi[0]+Ci[ 7]*Fi[1]+Ci[ 8]*Fi[2]+Ci[ 9]*Fi[3]+Ci[13]*Fi[4];
  auto _4 = Ci[10]*Fi[0]+Ci[11]*Fi[1]+Ci[12]*Fi[2]+Ci[13]*Fi[3]+Ci[14]*Fi[4];
  *ti = Fi[ 0]*_0 + Fi[ 1]*_1 + Fi[ 2]*_2 + Fi[ 3]*_3 + Fi[ 4]*_4;
}
