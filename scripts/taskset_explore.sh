#!/bin/bash

PROGRAM=$1
PROCESSOR_COUNT=`cat /proc/cpuinfo | grep processor | tail -n1 | awk '{ print $3 }'`

echo Running scalability tests for program ${1}

mkdir -p output/${HOSTNAME}/scalability_$1
for i in `seq 0 ${PROCESSOR_COUNT}`;
do
  echo $i
  taskset -c 0-${i} ./$1 -n 50000 > output/${HOSTNAME}/scalability_$1/${i}.out
done

