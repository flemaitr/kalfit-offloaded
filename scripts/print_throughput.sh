CORE_COUNT=$1

if [ $# -lt 1 ] ; then
  echo "Usage: print core_count"
  exit 0
fi

# Parse results when complete
cd output

# Empty files
echo '' > forward_times.txt
echo '' > backward_times.txt
echo '' > bismooth_times.txt

# Calculate average times
for i in `seq 1 $CORE_COUNT`
do
  cat $i/output.txt | grep "Forward fit mean:" | awk '{print $6}' >> forward_times.txt
  cat $i/output.txt | grep "Backward fit mean:" | awk '{print $6}' >> backward_times.txt
  cat $i/output.txt | grep "Bismooth fit mean:" | awk '{print $6}' >> bismooth_times.txt
done
TIME_AVG_FORWARD=`cat forward_times.txt | awk '{sum += $1} END {print sum / NR}'`
TIME_AVG_BACKWARD=`cat backward_times.txt | awk '{sum += $1} END {print sum / NR}'`
TIME_AVG_BISMOOTH=`cat bismooth_times.txt | awk '{sum += $1} END {print sum / NR}'`

printf "\nTime averages:\n"\
"Forward fit: $TIME_AVG_FORWARD s\n"\
"Backward fit: $TIME_AVG_BACKWARD s\n"\
"Bismooth fit: $TIME_AVG_BISMOOTH s\n" | tee -a average_times.txt

# Attempt to get also throughput
FILTERED_EXECUTED=`cat 1/output.txt | grep "Total statistics:" | awk '{print $3}'`
PREDICTED_EXECUTED=`cat 1/output.txt | grep "Total statistics:" | awk '{print $5}'`
BISMOOTHED_EXECUTED=`cat 1/output.txt | grep "Total statistics:" | awk '{print $7}'`

echo $FILTERED_EXECUTED $PREDICTED_EXECUTED $CORE_COUNT $TIME_AVG_FORWARD $TIME_AVG_BACKWARD \
  | awk '{print "Machine reconstruction throughput:", (($1+$2)*$3)/(2*($4+$5)), "filter+predict/s"}' | tee -a average_times.txt

echo $BISMOOTHED_EXECUTED $CORE_COUNT $TIME_AVG_BISMOOTH | awk '{print "Machine reconstruction throughput:", ($1*$2)/$3, "bismoothed/s"}' | tee -a average_times.txt

cd ..
