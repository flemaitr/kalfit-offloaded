#!/bin/bash

OUTFILE=$2
DIR=$1

echo number of processors, fit mean, smoother mean, fit throughput, smoother throughput >> $OUTFILE
for i in `ls -rt $DIR | grep out`
do
  echo $i
  FIT_MEAN=`cat $DIR/$i | grep "Fit timers mean:" | awk '{ print $4 }'`
  FIT_THROUGHPUT=`cat $DIR/$i | grep "Forward and Backward fit:" | awk '{ print $8 }'`
  SMOOTHER_MEAN=`cat $DIR/$i | grep "Smoother timers mean:" | awk '{ print $4 }'`
  SMOOTHER_THROUGHPUT=`cat $DIR/$i | egrep "Smoother:" | awk '{ print $5 }'`
  PROC_NUM=`cat $DIR/$i | grep "tbb default_num_threads" | awk '{ print $6 }'`
  echo $PROC_NUM, $FIT_MEAN, $SMOOTHER_MEAN, $FIT_THROUGHPUT, $SMOOTHER_THROUGHPUT >> $OUTFILE
done

