#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <map>
#include <array>
#include <deque>
#include <unistd.h>

#include "oldfit/Types.h"
#include "oldfit/Predict.h"
#include "oldfit/Update.h"
#include "oldfit/Bismooth.h"

#include "utilities/Tools.h"
#include "utilities/Timer.h"
#include "utilities/FileReader.h"

// New types
#include "nfit/nTypes.h"
#include "nfit/nPredict.h"
#include "nfit/nUpdate.h"
#include "nfit/Scheduler.h"
#include "nfit/Smoother.h"

#define TBB_ON
// #define CALLGRIND_ON
#ifdef CALLGRIND_ON
#include <valgrind/callgrind.h>
#endif
#ifdef TBB_ON
#include "tbb/tbb.h"
#endif

#pragma message("Generating code for vector width " TO_STRING(VECTOR_WIDTH))

int main(int argc, char **argv) {
  std::string foldername = "events";
  std::vector<std::vector<Instance>> events;
  int bitEpsilonAllowed = 50;
  bool printFitNodesWhenDifferent = false;
  int numExperiments = 100;
  unsigned int maxNumFilesToOpen = 0;
  int minNumTracksInInstance = 0;
  bool checkResults = false;
  bool allEqual = true;
  unsigned verbose = 0;
  bool experimentMode = false;
  bool vectorised_opt = true;
  bool printTiming = false;
  unsigned minTrackBatchSize = 8;
  TimePrinter p (true);
  FileReader fr;
  char c;

  while (true) {
    c = getopt (argc, argv, "b:f:n:h?m:c:v:p:es:i:t:");
    if (c == -1 or c == 255 /* power8 */) break;

    switch (c) {
      case 'n':
        numExperiments = atoi(optarg);
        break;
      case 't':
        printTiming = (bool) atoi(optarg);
        break;
      case 'i':
        minTrackBatchSize = atoi(optarg);
        break;
      case 'f':
        foldername = optarg;
        break;
      case 'b':
        bitEpsilonAllowed = atoi(optarg);
        break;
      case 'm':
        maxNumFilesToOpen = atoi(optarg);
        break;
      case 'c':
        checkResults = (bool) atoi(optarg);
        break;
      case 'v':
        verbose = atoi(optarg);
        break;
      case 's':
        vectorised_opt = not ((bool) atoi(optarg));
        break;
      case 'p':
      {
        const unsigned int percentile = atoi(optarg);
        p = TimePrinter(true, percentile, 100 - percentile);
        break;
      }
      case 'e':
        experimentMode = true;
        break;
      case 'h':
      case '?':
        std::cout << "kalfit [-f foldername=events] [-n numExperiments=100]"
          << " [-s sequential=0 (vectorised)] [-i minTrackBatchSize=8]"
          << " [-m maxNumFilesToOpen=0 (all)] [-e (print git checkout version=false)]"
          << " [-p topBottomPercentile=3] [-t printTiming=0]"
          << " [-c checkResults=0 (0,1) [-b bitEpsilonAllowed=50] [-v verbose=0 (0,1,2)]]" << std::endl;
        return 0;
      default:
        break;
    }
  }

  if (experimentMode) {
    std::cout << "Reported version (git): " << shellexec("git rev-parse HEAD");
  }

  std::cout << "Running options: ";
  for (int i=0; i<argc; ++i) std::cout << argv[i] << " ";
  std::cout << std::endl << std::endl;

  // Read events
  GrowingMemManager memManager (41);
  {
    // files will be deleted - we don't want to waste a lot of memory on this
    std::vector<std::vector<uint8_t>> files;
    fr.readFilesFromFolder(files, foldername, maxNumFilesToOpen);
    for (const auto& f : files) {
      events.push_back(translateFileIntoEvent(f, memManager, checkResults, minTrackBatchSize));
    }
  }

  assert(events.size() > 0);

  std::cout << "Read " << events.size() << " events" << std::endl;

  // Print some info for the events
  for (int i=0; i<events.size(); ++i) {
    const auto& event = events[i];

    std::vector<GroupInfo> groups;
    std::cout << "#" << i << ":" << std::endl;
    printInfo(event, groups);
  }

  // We should run "as the framework should"
  // So we run many events, scheduled with tbb,
  // exploiting parallelism in the best way possible
  // within each event, within a single core
  
  // Prepare the memory manager
  unsigned totalNodes = 0;
  size_t maxNodeLength = 0;

  for (auto& event : events) {
    for (auto& instance : event) {
      totalNodes = std::max(totalNodes, [&] { unsigned acc=0; for (const auto& t : instance.ntracks) acc += t.m_nodes.size(); return acc; } ());
      maxNodeLength = std::max(maxNodeLength, [&] { size_t m=0; for (const auto& t : instance.ntracks) m = std::max(m, t.m_nodes.size()); return m; } ()) / 2.0;
    }
  }
  const unsigned reservedMem = totalNodes + maxNodeLength * VECTOR_WIDTH;
  const unsigned reservedMemSeq = vectorised_opt ? reservedMem : reservedMem * 2;
  const unsigned reservedMemVec = vectorised_opt ? reservedMem : 1;

#ifdef TBB_ON
  tbb::enumerable_thread_specific<MemManager> threadmemseq = MemManager(41, reservedMemSeq);
  tbb::enumerable_thread_specific<MemManager> threadmemvecforward = MemManager(41, reservedMemVec);
  tbb::enumerable_thread_specific<MemManager> threadmemvecbackward = MemManager(41, reservedMemVec);
  tbb::enumerable_thread_specific<MemManager> threadmemvecpost = MemManager(41, reservedMemVec);
  tbb::enumerable_thread_specific<MemManager> threadmemvecsmoother = MemManager(22, reservedMem);
  tbb::mutex mutex;
#else
  MemManager memseq (41, reservedMemSeq);
  MemManager memvecforward (41, reservedMemVec);
  MemManager memvecbackward (41, reservedMemVec);
  MemManager memvecpost (41, reservedMemVec);
  MemManager memvecsmoother (22, reservedMem);
#endif

#ifdef TBB_ON
  std::vector<tbb::concurrent_vector<std::pair<int, Timer>>> timers (2);
#else
  std::vector<std::vector<std::pair<int, Timer>>> timers (2);
#endif

  std::cout << "Running experiments ..." << std::endl;

#ifdef TBB_ON
  tbb::parallel_for (static_cast<unsigned int>(0), static_cast<unsigned int>(numExperiments),
  [&] (unsigned int i) {
#else
  for (int i=0; i<numExperiments; ++i) {
#endif

    // Note: Copying the event requires too much memory, rather copy only what we need
    //       We copy so as not to pollute the events
    const unsigned int eventno = i % events.size(); // Round robin event selection
    const auto& event = events[eventno];

    int instno = 0;
    for (int j=0; j<event.size(); ++j) {
      const auto& instance = event[j];

      // Prepare experiment
      // Note: Copy the ntracks, which is exactly what we need for our experiment
      std::vector<nTrack> ntracks = instance.ntracks;
      const bool doForwardFit = instance.doForwardFit;
      const bool doBackwardFit = instance.doBackwardFit;
      const bool doBismooth = instance.doBismooth;
      const bool isOutlier = instance.isOutlier;
      std::vector<uint8_t> schedulingMasks {0xFF};
      std::vector<std::tuple<uint8_t, uint8_t, uint8_t, std::array<SchItem, VECTOR_WIDTH>>> scheduler;

      // Conditions for execution
      if (
        !isOutlier &&     // Outlier iterations would require data already prepared
                          // in memory as we require for the smoother
        doForwardFit &&   // This was an oversight when creating the datatypes.
        doBackwardFit &&  // Forward, backward and smoother are always requested together.
        doBismooth &&     // If this is not the case, do not execute
        ntracks.size() >= minTrackBatchSize // We set a minimum number of tracks. We don't care
                                            // that much about being completists, but rather about comparing
                                            // the performance between vectorised / non vectorised code
      ) {

        // Default to sequential execution if number of tracks is less than vector width
        const bool vectorised = (ntracks.size() >= VECTOR_WIDTH ? vectorised_opt : false);

#ifdef TBB_ON
        MemManager& memseq = threadmemseq.local();
        MemManager& memvecforward = threadmemvecforward.local();
        MemManager& memvecbackward = threadmemvecbackward.local();
        MemManager& memvecsmoother = threadmemvecsmoother.local();
        MemManager& memvecpost = threadmemvecpost.local();
#endif

        memseq.reset();
        memvecforward.reset();
        memvecbackward.reset();
        memvecsmoother.reset();
        memvecpost.reset();

        auto tFit = Timer();
        tFit.start();

#ifdef CALLGRIND_ON
        CALLGRIND_TOGGLE_COLLECT;
#endif

        // Dumb Simple Static Scheduler
        if (vectorised) {
          scheduler = DumbStaticScheduler::generate(ntracks, verbose>2);
        }

        // Forward fit
        // Initial iterations
        // Iterations until all tracks have info upstream
        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {

          std::vector<nFitNode>& track = ntrack.m_nodes;
          nFitNode& node = track.front();
          node.getFit<Fit::Forward>().m_states.setBasePointer(memseq.getNextElement());

          npredictInitialise<Fit::Forward>(node, ntrack.m_initialForwardCovariance);
          nupdate<Fit::Forward>(node);

          // Before m_forwardUpstream
          for (int i=1; i<=ntrack.m_forwardUpstream; ++i) {
            nFitNode& node = track[i];
            nFitNode& prevnode = track[i-1];
            node.getFit<Fit::Forward>().m_states.setBasePointer(memseq.getNextElement());
            
            npredict<Fit::Forward, false>(node, prevnode);
            nupdate<Fit::Forward>(node);
          }
        });
      
        if (!vectorised) {
          // Non-vectorised version
          std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
            std::vector<nFitNode>& track = ntrack.m_nodes;
            // After m_forwardUpstream
            for (int i=ntrack.m_forwardUpstream+1; i<track.size(); ++i) {
              nFitNode& node = track[i];
              nFitNode& prevnode = track[i-1];
              node.getFit<Fit::Forward>().m_states.setBasePointer(memseq.getNextElement());
              
              npredict<Fit::Forward, true>(node, prevnode);
              nupdate<Fit::Forward>(node);
            }
          });
        }
        else {
          // Vectorised version
          memvecforward.getNewVector();
          std::for_each (scheduler.begin(), scheduler.end(), [&] (decltype(scheduler[0])& s) {
            const uint8_t schInput  = std::get<0>(s);
            const uint8_t schOutput = std::get<1>(s);
            const uint8_t schAction = std::get<2>(s);
            auto& processingTracks  = std::get<3>(s);

            // Feed the data we need in our vector
            const auto& lastVector = memvecforward.getLastVector();
            if (schInput > 0x00) {
              for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                if (schInput & (1 << i)) {
                  nStates memslot (lastVector + i);
                  memslot.m_updatedState.copy(processingTracks[i].prevnode->getState<Fit::Forward, Fit::Updated, Fit::StateVector>());
                  memslot.m_updatedCovariance.copy(processingTracks[i].prevnode->getState<Fit::Forward, Fit::Updated, Fit::Covariance>());
                }
              }
            }

            // Update the pointers requested
            const auto& vector = memvecforward.getNewVector();
            for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
              if (schAction & (1 << i)) {
                processingTracks[i].node->getFit<Fit::Forward>().m_states.setBasePointer(vector + i);
              }
            }

            // predict and update
            nStates last (lastVector);
            nStates current (vector);

            npredict_vec<Fit::Forward>::op<VECTOR_WIDTH> (
              processingTracks,
              last.m_updatedState.basePointer,
              last.m_updatedCovariance.basePointer,
              current.m_predictedState.basePointer,
              current.m_predictedCovariance.basePointer
            );

            nupdate_vec<VECTOR_WIDTH> (
              processingTracks,
              current.m_predictedState.basePointer,
              current.m_predictedCovariance.basePointer,
              current.m_updatedState.basePointer,
              current.m_updatedCovariance.basePointer,
              current.m_chi2
            );

            // Move requested data out
            if (schOutput > 0x00) {
              for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                if (schOutput & (1 << i)) {
                  auto& item = processingTracks[i];

                  nStates memslot (memseq.getNextElement());
                  memslot.m_updatedCovariance.copy(item.node->getState<Fit::Forward, Fit::Updated, Fit::Covariance>());
                  memslot.m_updatedState.copy(item.node->getState<Fit::Forward, Fit::Updated, Fit::StateVector>());
                  item.node->getState<Fit::Forward, Fit::Updated, Fit::Covariance>().setBasePointer(memslot.m_updatedCovariance);
                  item.node->getState<Fit::Forward, Fit::Updated, Fit::StateVector>().setBasePointer(memslot.m_updatedState);
                }
              }
            }
          });

          // Last iterations
          std::vector<unsigned> slots (VECTOR_WIDTH);
          std::iota(slots.begin(), slots.end(), 0);
          {
            // Vectorised version
            // Attempt to take the tracks in batches of vector_size
            std::array<SchItem, VECTOR_WIDTH> processingTracks;
            memvecpost.getNewVector();

            auto trackIterator = ntracks.begin();
            while (trackIterator != ntracks.end()) {
              // Grab a track more
              nTrack& ntrack = *trackIterator;
              std::vector<nFitNode>& track = ntrack.m_nodes;
              const int slot = slots.back();
              slots.pop_back();
              
              processingTracks[slot] = SchItem (
                &track,
                ntrack.m_nodes.size() - ntrack.m_backwardUpstream - 2,
                ntrack.m_nodes.size() - ntrack.m_backwardUpstream - 1,
                ntrack.m_backwardUpstream,
                ntrack.m_index
              );

              // Copy starting status in an empty slot
              nStates newslot (memvecpost.getLastVector() + slot);
              newslot.m_updatedState.copy(processingTracks[slot].prevnode->getState<Fit::Forward, Fit::Updated, Fit::StateVector>());
              newslot.m_updatedCovariance.copy(processingTracks[slot].prevnode->getState<Fit::Forward, Fit::Updated, Fit::Covariance>());

              while (slots.size() == 0) {
                // Start processing the nodes in batches
                // Prepare the storage location
                const auto& lastVector = memvecpost.getLastVector();
                const auto& vector = memvecpost.getNewVector();
                for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                  processingTracks[i].node->getFit<Fit::Forward>().m_states.setBasePointer(vector + i);
                }

                // predict and update
                nStates last (lastVector);
                nStates current (vector);

                npredict_vec<Fit::Forward>::op<VECTOR_WIDTH> (
                  processingTracks,
                  last.m_updatedState.basePointer,
                  last.m_updatedCovariance.basePointer,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer
                );

                nupdate_vec<VECTOR_WIDTH> (
                  processingTracks,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer,
                  current.m_updatedState.basePointer,
                  current.m_updatedCovariance.basePointer,
                  current.m_chi2
                );

                // Update all tracks information
                // and check for finished tracks
                for (unsigned k=0; k<VECTOR_WIDTH; ++k) {
                  SchItem& s = processingTracks[k];

                  if (s.node + 1 == s.track->end()) {
                    // Move data out and update pointers
                    nStates store (memseq.getNextElement());
                    store.m_updatedState.copy(s.node->getState<Fit::Forward, Fit::Updated, Fit::StateVector>());
                    store.m_updatedCovariance.copy(s.node->getState<Fit::Forward, Fit::Updated, Fit::Covariance>());
                    s.node->getState<Fit::Forward, Fit::Updated, Fit::StateVector>().setBasePointer(store.m_updatedState);
                    s.node->getState<Fit::Forward, Fit::Updated, Fit::Covariance>().setBasePointer(store.m_updatedCovariance);

                    // Free slot and remove finished track
                    slots.push_back(k);
                  } else {
                    // Advance to next node
                    ++s.prevnode;
                    ++s.node;
                  }
                };
              }

              trackIterator++;
            }

            // We have a number of resting nodes
            if (slots.size() < VECTOR_WIDTH) {
              // Have an action mask, following our previous design
              uint8_t actionMask = ArrayGen::mask<VECTOR_WIDTH>();
              std::for_each (slots.begin(), slots.end(), [&actionMask] (decltype(slots[0])& i) {
                actionMask &= ~(1 << i);
              });

              // We will finish once all slots are used up
              while (actionMask > 0x00) {
                const auto& lastVector = memvecpost.getLastVector();
                const auto& vector = memvecpost.getNewVector();
                for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                  if (actionMask & (1 << i)) {
                    processingTracks[i].node->getFit<Fit::Forward>().m_states.setBasePointer(vector + i);
                  }
                }
                
                // predict and update
                nStates last (lastVector);
                nStates current (vector);
                
                npredict_vec<Fit::Forward>::op<VECTOR_WIDTH> (
                  processingTracks,
                  last.m_updatedState.basePointer,
                  last.m_updatedCovariance.basePointer,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer
                );

                nupdate_vec<VECTOR_WIDTH> (
                  processingTracks,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer,
                  current.m_updatedState.basePointer,
                  current.m_updatedCovariance.basePointer,
                  current.m_chi2
                );

                // Check for finished tracks
                for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                  if (actionMask & (1 << i)) {
                    SchItem& s = processingTracks[i];

                    if (s.node + 1 == s.track->end()) {
                      actionMask &= ~(1 << i);
                    } else {
                      ++s.prevnode;
                      ++s.node;
                    }
                  }
                }
              }
            }
          }
        }

        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
          std::vector<nFitNode>& track = ntrack.m_nodes;
          nFitNode& node = track.back();
          node.getFit<Fit::Backward>().m_states.setBasePointer(memseq.getNextElement());

          npredictInitialise<Fit::Backward>(node, ntrack.m_initialBackwardCovariance);
          nupdate<Fit::Backward>(node);

          // Before m_backwardUpstream
          for (int i=1; i<=ntrack.m_backwardUpstream; ++i) {
            const int element = ntrack.m_nodes.size() - i - 1;
            nFitNode& prevnode = track[element + 1];
            nFitNode& node = track[element];
            node.getFit<Fit::Backward>().m_states.setBasePointer(memseq.getNextElement());
            
            npredict<Fit::Backward, false>(node, prevnode);
            nupdate<Fit::Backward>(node);
          }
        });

        if (!vectorised) {
          // Non vectorised version
          std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
            std::vector<nFitNode>& track = ntrack.m_nodes;
            // After m_backwardUpstream
            for (int i=ntrack.m_backwardUpstream+1; i<track.size(); ++i) {
              const int element = ntrack.m_nodes.size() - i - 1;
              nFitNode& node = track[element];
              nFitNode& prevnode = track[element + 1];
              node.getFit<Fit::Backward>().m_states.setBasePointer(memseq.getNextElement());
              
              npredict<Fit::Backward, true>(node, prevnode);
              nupdate<Fit::Backward>(node);
            }
          });
        } else {
          // Vectorised version
          std::vector<SwapStore> swaps;
          memvecbackward.getNewVector();
          
          // Smoother
          nStates forward (memvecforward.getLastVector());

          std::for_each (scheduler.rbegin(), scheduler.rend(), [&] (decltype(scheduler[0])& s) {
            const uint8_t schOutput = std::get<0>(s);
            const uint8_t schInput  = std::get<1>(s);
            const uint8_t schAction = std::get<2>(s);
            auto& processingTracks  = std::get<3>(s);

            // TODO: Remove this... I still don't know how exactly
            for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
              processingTracks[i].prevnode = processingTracks[i].node + 1;
            }

            // Feed the data we need in our vector
            const auto& lastVector = memvecbackward.getLastVector();
            if (schInput > 0x00) {
              for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                if (schInput & (1 << i)) {
                  nStates memslot (lastVector + i);
                  memslot.m_updatedState.copy(processingTracks[i].prevnode->getState<Fit::Backward, Fit::Updated, Fit::StateVector>());
                  memslot.m_updatedCovariance.copy(processingTracks[i].prevnode->getState<Fit::Backward, Fit::Updated, Fit::Covariance>());
                }
              }
            }

            // Update the pointers requested
            const auto& vector = memvecbackward.getNewVector();
            for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
              if (schAction & (1 << i)) {
                processingTracks[i].node->getFit<Fit::Backward>().m_states.setBasePointer(vector + i);
              }
            }

            // predict and update
            nStates last (lastVector);
            nStates current (vector);

            npredict_vec<Fit::Backward>::op<VECTOR_WIDTH> (
              processingTracks,
              last.m_updatedState.basePointer,
              last.m_updatedCovariance.basePointer,
              current.m_predictedState.basePointer,
              current.m_predictedCovariance.basePointer
            );

            nupdate_vec<VECTOR_WIDTH> (
              processingTracks,
              current.m_predictedState.basePointer,
              current.m_predictedCovariance.basePointer,
              current.m_updatedState.basePointer,
              current.m_updatedCovariance.basePointer,
              current.m_chi2
            );

            // Do the smoother *here*
            // This allows for cache benefits,
            // and removes the need of doing swapping to restore
            // the updated states
            
            // Fetch a new memvecsmoother vector
            const auto& smootherVector = memvecsmoother.getNewVector();
            SmoothState smooth (smootherVector);

            // Update pointers
            for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
              if (schAction & (1 << i)) {
                processingTracks[i].node->m_smoothState.setBasePointer(smootherVector + i);
              }
            }

            // Smoother
            uint8_t smoother_result = smoother_vec<VECTOR_WIDTH> (
              forward.m_predictedState.basePointer,
              forward.m_predictedCovariance.basePointer,
              current.m_updatedState.basePointer,
              current.m_updatedCovariance.basePointer,
              smooth.m_state.basePointer,
              smooth.m_covariance.basePointer
            );

            // Update residuals
            updateResiduals_vec (
              processingTracks,
              smooth.m_state.basePointer,
              smooth.m_covariance.basePointer,
              smooth.m_residual,
              smooth.m_errResidual
            );

            // Move to the next vector
            --forward;

            // Move requested data out
            if (schOutput > 0x00) {
              for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                if (schOutput & (1 << i)) {
                  auto& item = processingTracks[i];

                  nStates memslot (memseq.getNextElement());
                  memslot.m_updatedState.copy(item.node->getState<Fit::Backward, Fit::Updated, Fit::StateVector>());
                  memslot.m_updatedCovariance.copy(item.node->getState<Fit::Backward, Fit::Updated, Fit::Covariance>());
                  item.node->getState<Fit::Backward, Fit::Updated, Fit::StateVector>().setBasePointer(memslot.m_updatedState);
                  item.node->getState<Fit::Backward, Fit::Updated, Fit::Covariance>().setBasePointer(memslot.m_updatedCovariance);
                }
              }
            }
          });

          // Last iterations
          std::vector<unsigned> slots (VECTOR_WIDTH);
          std::iota(slots.begin(), slots.end(), 0);
          {
            std::array<SchItem, VECTOR_WIDTH> processingTracks;
            memvecpost.getNewVector();
            auto trackIterator = ntracks.begin();
            while (trackIterator != ntracks.end()) {
              nTrack& ntrack = *trackIterator;
              std::vector<nFitNode>& track = ntrack.m_nodes;
              const int slot = slots.back();
              slots.pop_back();
              
              processingTracks[slot] = SchItem (
                &track,
                ntrack.m_forwardUpstream + 1,
                ntrack.m_forwardUpstream,
                ntrack.m_backwardUpstream,
                ntrack.m_index
              );

              nStates newslot (memvecpost.getLastVector() + slot);
              newslot.m_updatedState.copy(processingTracks[slot].prevnode->getState<Fit::Backward, Fit::Updated, Fit::StateVector>());
              newslot.m_updatedCovariance.copy(processingTracks[slot].prevnode->getState<Fit::Backward, Fit::Updated, Fit::Covariance>());

              while (slots.size() == 0) {
                const auto& lastVector = memvecpost.getLastVector();
                const auto& vector = memvecpost.getNewVector();
                for (int i=0; i<VECTOR_WIDTH; ++i) {
                  processingTracks[i].node->getFit<Fit::Backward>().m_states.setBasePointer(vector + i);
                }

                nStates last (lastVector);
                nStates current (vector);

                npredict_vec<Fit::Backward>::op<VECTOR_WIDTH> (
                  processingTracks,
                  last.m_updatedState.basePointer,
                  last.m_updatedCovariance.basePointer,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer
                );

                nupdate_vec<VECTOR_WIDTH> (
                  processingTracks,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer,
                  current.m_updatedState.basePointer,
                  current.m_updatedCovariance.basePointer,
                  current.m_chi2
                );

                for (unsigned k=0; k<VECTOR_WIDTH; ++k) {
                  SchItem& s = processingTracks[k];

                  if (s.node == s.track->begin()) {
                    nStates store (memseq.getNextElement());
                    store.m_updatedState.copy(s.node->getState<Fit::Backward, Fit::Updated, Fit::StateVector>());
                    store.m_updatedCovariance.copy(s.node->getState<Fit::Backward, Fit::Updated, Fit::Covariance>());
                    s.node->getState<Fit::Backward, Fit::Updated, Fit::StateVector>().setBasePointer(store.m_updatedState);
                    s.node->getState<Fit::Backward, Fit::Updated, Fit::Covariance>().setBasePointer(store.m_updatedCovariance);
                    slots.push_back(k);
                  } else {
                    --s.prevnode;
                    --s.node;
                  }
                };
              }

              trackIterator++;
            }

            if (slots.size() < VECTOR_WIDTH) {
              uint8_t actionMask = ArrayGen::mask<VECTOR_WIDTH>();
              std::for_each (slots.begin(), slots.end(), [&actionMask] (decltype(slots[0])& i) {
                actionMask &= ~(1 << i);
              });

              while (actionMask > 0x00) {
                const auto& lastVector = memvecpost.getLastVector();
                const auto& vector = memvecpost.getNewVector();
                for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                  if (actionMask & (1 << i)) {
                    processingTracks[i].node->getFit<Fit::Backward>().m_states.setBasePointer(vector + i);
                  }
                }

                nStates last (lastVector);
                nStates current (vector);

                npredict_vec<Fit::Backward>::op<VECTOR_WIDTH> (
                  processingTracks,
                  last.m_updatedState.basePointer,
                  last.m_updatedCovariance.basePointer,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer
                );

                // update
                nupdate_vec<VECTOR_WIDTH> (
                  processingTracks,
                  current.m_predictedState.basePointer,
                  current.m_predictedCovariance.basePointer,
                  current.m_updatedState.basePointer,
                  current.m_updatedCovariance.basePointer,
                  current.m_chi2
                );

                for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
                  if (actionMask & (1 << i)) {
                    SchItem& s = processingTracks[i];

                    if (s.node == s.track->begin()) {
                      actionMask &= ~(1 << i);
                    } else {
                      --s.prevnode;
                      --s.node;
                    }
                  }
                }
              }
            }
          }
        }

        // Calculate the chi2 a posteriori
        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
          std::vector<nFitNode>& track = ntrack.m_nodes;
          ntrack.m_ndof = -track[0].m_parent_nTrackParameters;
          for (int k=0; k<track.size(); ++k) {
            const nFitNode& node = track[k];

            if (node.m_type == HitOnTrack) {
              ++ntrack.m_ndof;
              ntrack.m_forwardFitChi2  += node.getChi2<Fit::Forward>();
              ntrack.m_backwardFitChi2 += node.getChi2<Fit::Backward>();
            }
          }
        });

        tFit.stop();

        // Smoother
        auto tSmoother = Timer();
        tSmoother.start();

        // Not upstream iterations
        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
          for (int j=0; j<=ntrack.m_forwardUpstream; ++j) {
            ntrack.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
            smoother<false, true>(ntrack.m_nodes[j]);
            updateResiduals(ntrack.m_nodes[j]);
          }

          for (int j=0; j<=ntrack.m_backwardUpstream; ++j) {
            const unsigned element = ntrack.m_nodes.size() - j - 1;
            ntrack.m_nodes[element].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
            smoother<true, false>(ntrack.m_nodes[element]);
            updateResiduals(ntrack.m_nodes[element]);
          }
        });

        if (!vectorised) {
          std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
            for (int j = ntrack.m_forwardUpstream + 1; j < (ntrack.m_nodes.size() - ntrack.m_backwardUpstream - 1); ++j) {
              ntrack.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
              smoother<true, true>(ntrack.m_nodes[j]);
              updateResiduals(ntrack.m_nodes[j]);
            }
          });
        }
        tSmoother.stop();

        unsigned numberOfSmoothers = 0;
        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) { numberOfSmoothers += ntrack.m_nodes.size(); });
        timers[1].push_back(std::make_pair(numberOfSmoothers, tSmoother));

#ifdef CALLGRIND_ON
        CALLGRIND_TOGGLE_COLLECT;
#endif

        // Add fit timer to list
        unsigned numberOfFits = 0;
        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
          numberOfFits += 2 * ntrack.m_nodes.size();
        });
        timers[0].push_back(std::make_pair(numberOfFits, tFit));
      
        // Convert back
        if (checkResults) {
          for (const auto& ntrack : ntracks) {
            const std::vector<FitNode>& result = instance.expectedResult[ntrack.m_index];

            std::vector<FitNode> track;
            for (const auto& node : ntrack.m_nodes) {
              track.push_back(node);
            }
            track.back().m_totalChi2[Forward] = ChiSquare(ntrack.m_forwardFitChi2, ntrack.m_ndof);
            track.front().m_totalChi2[Backward] = ChiSquare(ntrack.m_backwardFitChi2, ntrack.m_ndof);
#ifdef TBB_ON
            tbb::mutex::scoped_lock lock (mutex);
#endif
            for (int l=0; l<track.size(); ++l) {
              int compareChi2 = -1;
              if (l==0) compareChi2 = Backward;
              else if (l==track.size()-1) compareChi2 = Forward;

              const bool comparison = compare(track[l], result[l], pow(2.0, ((double) bitEpsilonAllowed)), (verbose > 0), compareChi2);
              if (!comparison) {
                std::cout << "Mismatch in event #" << eventno << ", instance #" << j
                  << " (" << doForwardFit << ", " << doBackwardFit << ", " << doBismooth << ")"
                  << ", track " << ntrack.m_index << ", node " << l << std::endl;
                allEqual = false;

                if (verbose > 1) {
                  print(track[l]);
                  std::cout << std::endl;
                  print(result[l]);
                }
              }
            }
          }
        }
      }
    }
#ifdef TBB_ON
  });
#else
  }
#endif
  
#ifdef CALLGRIND_ON
  CALLGRIND_DUMP_STATS;
#endif

  // Get estimate on number of threads being used by tbb
#ifdef TBB_ON
  const int numberOfThreads = tbb::task_scheduler_init::default_num_threads();
#else
  const int numberOfThreads = 1;
#endif

  std::cout << std::endl;

  std::map<std::string, double> fitsTimes     = p.printWeightedTimer(timers[0], "Forward and Backward fit", printTiming);
  std::map<std::string, double> smootherTimes = p.printWeightedTimer(timers[1], "Smoother", printTiming);

  const unsigned long long totalFitted   = [&] {unsigned long long t=0; for (const auto& wt : timers[0]) t += wt.first; return t;} ();
  const unsigned long long totalSmoothed = [&] {unsigned long long t=0; for (const auto& wt : timers[1]) t += wt.first; return t;} ();

  std::cout << "Total statistics: " << totalFitted << " fitted, " << totalSmoothed << " smoothed" << std::endl << std::endl;

  if (checkResults) {
    if (allEqual) {
      std::cout << "Check results succeeded!" << std::endl
        << "All of the experiments gave the expected results" << std::endl << std::endl;
    }
    else {
      std::cout << "Check results failed" << std::endl
        << "Some experiments did not yield the expected results" << std::endl
        << "Try running with -c 1 -v 1 to see what happens" << std::endl << std::endl;
    }
  }

  std::cout << "Fit timers mean: " << fitsTimes["mean"] << " sum: " << fitsTimes["sum"]
    << " min: " << fitsTimes["min"] << " max: " << fitsTimes["max"] << " stddev: " << fitsTimes["stdev"]
    << " rawsum: " << fitsTimes["rawsum"] << std::endl
    << "Smoother timers mean: " << smootherTimes["mean"] << " sum: " << smootherTimes["sum"]
    << " min: " << smootherTimes["min"] << " max: " << smootherTimes["max"] << " stddev: " << smootherTimes["stdev"]
    << " rawsum: " << smootherTimes["rawsum"] << std::endl;

  std::cout << std::endl
    << "tbb default_num_threads reports execution with " << numberOfThreads << " threads" << std::endl
    << "Throughput per processor, estimated total throughput:" << std::endl;

  // New metric:
  const unsigned long long totalFullFitAndSmooth = totalSmoothed;
  const double fullTime = fitsTimes["rawsum"] + smootherTimes["rawsum"];
  const double fullMean = fullTime / ((double) totalFullFitAndSmooth);
  std::cout << " Fits and smoother combined: " << 1.0 / fullMean << " / s, "
    << numberOfThreads * (1.0 / fullMean) << " / s" << std::endl;

  return 0;
}
